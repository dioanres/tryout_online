//$(document).ready(function(){
    //$('#tbl-guru').DataTable();
//});
//var base_url = "<?= base_url() ?>";
var soal = '';

function CountDown(duration, display) {
    if (!isNaN(duration)) {
        var timer = duration, minutes, seconds;
        
      var interVal=  setInterval(function () {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            $(display).html("<b>" + minutes + "m : " + seconds + "s" + "</b>");
            if (--timer < 0) {
                timer = duration;
               selesai_ujian();
               $('#display').empty();
               clearInterval(interVal)
            }
            },1000);
    }
}

//edit rn
$('#btn-simpan-soal').click(function(){
    a = $('#txt-soal').val();

    var soal = CKEDITOR.instances['txt-soal'].getData();
    var a = CKEDITOR.instances['pilihan-a'].getData();
    var b = CKEDITOR.instances['pilihan-b'].getData();
    var c = CKEDITOR.instances['pilihan-c'].getData();
    var d = CKEDITOR.instances['pilihan-d'].getData();

    var id_ujian = window.location.href.split("/").pop();

    $.ajax({
        url: base_url+"index.php/c_soal/tambah_text_soal",
        type: 'post',
        dataType: 'json',
        async:false,
        data: {
            mapel: $('#slc-pelajaran-soal').val(),
            guru:$('#txt-guru-soal').val(),
            soal:soal,
            pil_a: a,
            pil_b: b,
            pil_c: c,
            pil_d: d,
            jawaban: $('#jawaban').val(),
            id_ujian: id_ujian
        },
        cache: false,
        success: function(response, status, error,pesan) {
           console.log(response,status,error,pesan);
           if (response===1) {
            //alert('Sukses');
            alert_info('Info','Data Berhasil Disimpan');
            window.location.href=base_url+'index.php/c_ujian';
            
           } else {
            alert ('Gagal');
           }

           //ajax_list_guru();
               },
        error:function(response){
            console.log(response);
        }
    });
});

function selesai_ujian(){
    var hasil = [];
    var jawaban = '';
    $.each(soal, function(i){
        if ($('input[name=jwb-'+this['id_soal']+']:checked').val() != undefined){
            jawaban = $('input[name=jwb-'+this['id_soal']+']:checked').val();
        }
        hasil[i] = {
            jawaban: jawaban,
            soal: this['id_soal'],
            kunci: this['jawaban']
        }
    });
    
    $.ajax({
        url: base_url+"index.php/c_soal/simpan_jawaban",
        dataType: 'json',
        type:'post',
        data:{
            jawaban:hasil,
            id_ujian: localStorage.getItem('id_ujian')
        },
        cache: false,
        async:false,
        success: function(response, status, error,pesan) {
           console.log(response);
           window.location.replace(base_url+'index.php/c_ujian/proses_ujian');
        },
        error:function(response){
            console.log(response['responseText']);
            
        }
    });
}

$('#btn-selesai-ujian').click(function(){
    selesai_ujian();
});

if(window.location.href.includes('c_ujian/hasil_ujian')){
    var tabel_hasil_ujian = $('#tbl-hasil-ujian').DataTable({
        "responsive": true,
       "paginate": true,
        "lengthChange": false,
        "info": false,
        "autoWidth": false,
        "displayLength": 10,
        "filter": false,
        "paging": true,
    });

    function list_hasil_ujian(){
        $.ajax({
            url: base_url+"index.php/c_ujian/list_hasil_ujian",
            dataType: 'json',
            type:'post',
            cache: false,
            async:false,
            success: function(response, status, error,pesan) {
               console.log(response);
               tabel_hasil_ujian.clear().draw();
               soal = response;
               $.each(response, function(i) {
                    var aksi = "<a class='btn-edit-soal green' href=base_url+'index.php/c_ujian/view_hasil_ujian_siswa/"+this['id_ujian']+"'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a>";
                    tabel_hasil_ujian.row.add([
                        i+1,
                        this['nama_ujian'],
                        this['nama_pelajaran'],
                        this['jumlah'],
                        this['waktu'],
                        aksi
                    ]).draw(false);
                });
            },
            error:function(response){
                console.log(response['responseText']);
                
            }
        });
    }

    list_hasil_ujian();
}

if(window.location.href.includes('c_ujian/view_hasil_ujian_siswa')){
    var tabel_ujian_siswa = $('#tbl-hasil-ujian-siswa').DataTable({
        "responsive": true,
       "paginate": true,
        "lengthChange": false,
        "info": false,
        "autoWidth": false,
        "displayLength": 10,
        "filter": false,
        "paging": true,
    });

    function list_hasil_ujian_siswa(){
        localStorage.setItem('id_ujian', window.location.href.split("/").pop());
        $.ajax({
            url: base_url+"index.php/c_ujian/list_hasil_ujian_detail",
            dataType: 'json',
            data:{
                id_ujian: window.location.href.split("/").pop()
            },
            type:'post',
            cache: false,
            async:false,
            success: function(response, status, error,pesan) {
               console.log(response);
               tabel_ujian_siswa.clear().draw();
               $.each(response, function(i) {
                    var aksi = "<a class='green' href=base_url+'index.php/c_ujian/list_hasil_ujian_detail_siswa/"+this['id_admin']+"'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a>";
                    tabel_ujian_siswa.row.add([
                        this['nama_admin'],
                        this['score'],
                        this['Keterangan'],
                        aksi
                    ]).draw(false);
                    i++;
                    
                });
            },
            error:function(response){
                console.log(response['responseText']);
                
            }
        });
    }

    list_hasil_ujian_siswa();
}

if(window.location.href.includes('c_ujian/list_hasil_ujian_detail_siswa')){
    function ajax_list_soal_ujian(){
        console.log('list soal ujian');
        console.log(localStorage.getItem('id_ujian'));
        //tabel_guru.clear().draw()
        $.ajax({
            url: base_url+"index.php/c_ujian/list_soal_ujian_siswa",
            dataType: 'json',
            data:{
                id_ujian:localStorage.getItem('id_ujian')
            },
            type:'post',
            cache: false,
            async:false,
            success: function(response, status, error,pesan) {
               console.log(response);
               var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
               
               $('#div-ujian-nama').html(response[0]['nama_ujian']);
               $('#div-mapel').html(response[0]['nama_pelajaran']);
               /*var random_index = Math.floor(Math.random()*data.length);
               var item = data[random_index];*/
               $.each(response, function(i) {
                    $('#div-ujian').append('<div id = "soal-'+this['id_soal']+'"></div>')
                    $('#soal-'+this['id_soal']).append(i + 1 + '. ' + this['soal']);
                    $('#soal-'+this['id_soal']).append('<p class = "jawaban"> <label><input id="A'+this['id_soal']+'" value="A" disabled type="radio" name="jwb-'+this['id_soal']+'"> ' + this['pil_a'] + '</p></label>');
                    $('#soal-'+this['id_soal']).append('<p class = "jawaban"> <label><input id="B'+this['id_soal']+'" value="B" disabled type="radio" name="jwb-'+this['id_soal']+'"> ' + this['pil_b'] + '</p></label>');
                    $('#soal-'+this['id_soal']).append('<p class = "jawaban"> <label><input id="C'+this['id_soal']+'" value="C" disabled type="radio" name="jwb-'+this['id_soal']+'"> ' + this['pil_c'] + '</p></label>');
                    $('#soal-'+this['id_soal']).append('<p class = "jawaban"> <label><input id="D'+this['id_soal']+'" value="D" disabled type="radio" name="jwb-'+this['id_soal']+'"> ' + this['pil_d'] + '</p></label>');
                    $('#soal-'+this['id_soal']).append('<p class = "jawaban"> <label>Kunci Jawaban: '+this['jawaban']+'</p></label>');
                    $('#soal-'+this['id_soal']).css('margin-bottom', '10px');
                    $('#'+this['jwbn']+this['id_soal']).prop('checked', true);
                });
               $('.jawaban').css('margin-bottom', '0px');
               $('.jawaban').css('margin-left', '20px');
               console.log(response);
            },
            error:function(response){
                console.log(response['responseText']);
                
            }
        }); 
    }

    ajax_list_soal_ujian();
}

if(window.location.href.includes('c_soal/update_soal')){
    var tabel_soal = $('#tbl-soal').DataTable({
        "responsive": true,
       "paginate": true,
        "lengthChange": false,
        "info": false,
        "autoWidth": false,
        "displayLength": 10,
        "filter": false,
        "paging": true,
    });

    function list_soal_edit(){
        $.ajax({
            url: base_url+ "index.php/c_soal/list_soal_ujian",
            dataType: 'json',
            data:{
                id_ujian: window.location.href.split("/").pop()
            },
            type:'post',
            cache: false,
            async:false,
            success: function(response, status, error,pesan) {
               console.log(response);
               tabel_soal.clear().draw();
               soal = response;
               var aksi = "<span class='btn-edit-soal green' href=''><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></span><span class='red btn-delete-soal' href=''><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></span>";
               $.each(response, function(i) {
                    tabel_soal.row.add([
                        this['soal'],
                        aksi,
                        this['pil_a'],
                        this['pil_b'],
                        this['pil_c'],
                        this['pil_d'],
                        this['jawaban'],
                        this['id_soal']
                    ]).draw(false);
                    i++;
                    
                });
            },
            error:function(response){
                console.log(response['responseText']);
                
            }
        });
    }

    list_soal_edit();

    $('#tbl-soal tbody').on('click', '.btn-edit-soal', function() {
        $(this).addClass('selected');
        if (typeof tabel_soal.row($(this).closest('tr')).data() !== 'undefined') {
            arr_soal = tabel_soal.row($(this).closest('tr')).data();
        } else if (typeof tabel_soal.row(this).data() !== 'undefined') {
            arr_soal = tabel_soal.row(this).data();
        } else {
            arr_soal = tabel_soal.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }

        $('#modal-soal').modal('show');

        CKEDITOR.instances['txt-soal'].setData(arr_soal[0]);
        CKEDITOR.instances['pilihan-a'].setData(arr_soal[2]);
        CKEDITOR.instances['pilihan-b'].setData(arr_soal[3]);
        CKEDITOR.instances['pilihan-c'].setData(arr_soal[4]);
        CKEDITOR.instances['pilihan-d'].setData(arr_soal[5]);
        $('#jawaban').val(arr_soal[6]);
        $('#id-soal-hidden').val(arr_soal[7]);
        $('#id-soal-hidden').hide();
    });

    $('#tbl-soal tbody').on('click', '.btn-delete-soal', function() {
        $(this).addClass('selected');
        if (typeof tabel_soal.row($(this).closest('tr')).data() !== 'undefined') {
            arr_soal = tabel_soal.row($(this).closest('tr')).data();
        } else if (typeof tabel_soal.row(this).data() !== 'undefined') {
            arr_soal = tabel_soal.row(this).data();
        } else {
            arr_soal = tabel_soal.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }

        
        $('#id-soal-hidden').val();
        $.confirm({
            title: 'Konfirmasi',
            content: 'Yakin Ingin Hapus Soal?',
            buttons: {
                confirm: function () {
                    ajax_delete(arr_soal[7],base_url+"index.php/c_soal/hapus_soal");            
                    //$.alert('Soal Berhasil dihapus!');
                    list_soal_edit();
                },
                cancel: function () {
                    //$.alert('Canceled!');
                    list_soal_edit();
                }
            }
        });
    });

    $('#btn-edit-soal').click(function(){
        var soal = CKEDITOR.instances['txt-soal'].getData();
        var a = CKEDITOR.instances['pilihan-a'].getData();
        var b = CKEDITOR.instances['pilihan-b'].getData();
        var c = CKEDITOR.instances['pilihan-c'].getData();
        var d = CKEDITOR.instances['pilihan-d'].getData();

        $.ajax({
            url: base_url+"index.php/c_soal/edit_text_soal",
            type: 'post',
            dataType: 'json',
            async:false,
            data: {
                mapel: $('#slc-pelajaran-soal').val(),
                guru:$('#txt-guru-soal').val(),
                soal:soal,
                pil_a: a,
                pil_b: b,
                pil_c: c,
                pil_d: d,
                jawaban: $('#jawaban').val(),
                id_soal: $('#id-soal-hidden').val()
            },
            cache: false,
            success: function(response, status, error,pesan) {
               console.log(response,status,error,pesan);
               if (response===1) {
                $.alert('Soal Berhasil diubah!');
                list_soal_edit();
               } else {
                alert ('Gagal');
               }

               //ajax_list_guru();
                   },
            error:function(response){
                console.log(response);
            }
        });
    });
}