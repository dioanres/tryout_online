//Login

//console.log(last_cat);



$( document ).ready(function() {
	var tabel_guru = $('#tbl-guru').DataTable({
	    "responsive": true,
	   /* "columnDefs": [{
	        "targets": -1,
	        "data": null,
	        "defaultContent": aksi_alamat_com
	    }, {
	        "targets": [1],
	        "visible": false,
	        "searchable": false
	    }],*/
	    "paginate": true,
	    "lengthChange": false,
	    //"bFilter": false,
	    "info": false,
	    "autoWidth": false,
	    "displayLength": 10,
	    "filter": false,
	    "paging": true,
	    //"info": true
	});
	var tabel_siswa = $('#tbl-siswa').DataTable({
	    "responsive": true,
	    "filter": true,
	    "paging": false,
	    "info": false
	});

	var tabel_mapel = $('#tbl-mapel').DataTable({
	    "responsive": true,
	    "filter": true,
	    "paging": false,
	    "info": false
	});

	var tabel_ujian = $('#tbl-ujian').DataTable({
	    "responsive": true,
	    "filter": true,
	    "paging": false,
	    "info": false,
	     "columnDefs": [{
        "targets": [1],
        "className": "hide"
    }]
	});

	var tabel_soal = $('#tbl-soal').DataTable({
	    "responsive": true,
	    "filter": true,
	    "paging": false,
	    "info": false,
	    
	});

	var tabel_soal_ujian = $('#tbl-soal-ujian').DataTable({
	    "responsive": true,
	    "filter": false,
	    "paging": false,
	    "info": false
	});

   
    var last_cat = window.location.href.split("/").pop();
    console.log(last_cat);
    if (last_cat ==='c_guru') {
    	ajax_list_guru();
    } else if (last_cat ==='c_siswa') {
    	ajax_list_siswa();
    } else if (last_cat==='c_mapel') {
    	ajax_list_mapel();
    } else if (last_cat==='proses_ujian') {
    	ajax_list_ujian_siswa();
    } else if (last_cat==='tambah_soal') {
    	ajax_param_mapel('#slc-pelajaran-soal');
    } else if (last_cat==='c_soal') {
    	ajax_list_soal();
    } else if (last_cat==='tampil_soal_ujian') {
    	ajax_list_soal_ujian();
    };

    var arr_soal=[];
    $('#tbl-ujian tbody').on('click', '.ikuti-ujian', function() {
    
	    $(this).addClass('selected');
	    if (typeof tabel_ujian.row($(this).closest('tr')).data() !== 'undefined') {
	        arr_soal = tabel_ujian.row($(this).closest('tr')).data();
	    } else if (typeof tabel_ujian.row(this).data() !== 'undefined') {
	        arr_soal = tabel_ujian.row(this).data();
	    } else {
	        arr_soal = tabel_ujian.row($(this).parent().parent().parent().parent().parent().prev()).data();
	    }
	    //console.log(tabel_ujian);
	    console.log(arr_soal);
	    localStorage.setItem('id_ujian', arr_soal[5]);

	    //localStorage.setItem('id_guru', tabel_ujian[1]);
	    location.href = 'http://localhost/newcat/index.php/c_ujian/tampil_soal_ujian';
	    //ajax_list_soal_ujian();

	    //console.log(localStorage.getItem('id_pel'));
	});

	var arr_guru=[];
    $('#tbl-guru tbody').on('click', '.dtl-guru', function() {
    	
    	$('#mod-guru').modal('show');
    	ajax_param_mapel('#mapel');
	    $(this).addClass('selected');
	    if (typeof tabel_guru.row($(this).closest('tr')).data() !== 'undefined') {
	        arr_guru = tabel_guru.row($(this).closest('tr')).data();
	    } else if (typeof tabel_guru.row(this).data() !== 'undefined') {
	        arr_guru = tabel_guru.row(this).data();
	    } else {
	        arr_guru = tabel_guru.row($(this).parent().parent().parent().parent().parent().prev()).data();
	    }
	    

	    localStorage.setItem('nip_guru', arr_guru[2]);
	    
	    ajax_guru_by_nip();
	    //location.href = 'http://localhost/newcat/index.php/c_ujian/tampil_soal_ujian';
	    //ajax_list_soal_ujian();

	    //console.log(localStorage.getItem('id_pel'));
	});


    $('#menu-guru').click(function(){
	//alert('guru');
	    console.log('list guru');
	    ajax_list_guru();
	});

	$('#menu-siswa').click(function(){
		ajax_list_siswa();
	});

	$('#menu-mapel').click(function(){
		ajax_list_mapel();
	});

	$('#tambah-guru').click(function(){
		$('#nip-guru').prop('disabled',false);
		ajax_param_mapel('#mapel');
	});

	$('#btn-tambah-soal').click(function(){
		//localStorage.setItem('id_pel',);
		ajax_param_mapel('#slc-pelajaran-soal');
	});

	$('#menu-proses-ujian').click(function(){
		ajax_list_ujian_siswa();
	});

	$('#menu-soal').click(function(){
		ajax_list_soal();
	});

	/*$('.dtl-guru').click(function(){
		$('#mod-guru').modal('show');
	});*/

    $.ajax({
    	url:"http://localhost/newcat/index.php/c_login/akses",
    	type:'post',
    	cache:false,
    	success:function(response) {
    		console.log(response);
    		if (response==='0') {
    			$('#menu-guru').show();
    			$('#menu-siswa').show();
    			$('#menu-mapel').show();
    		} else if (response==='1') {
    			$('#menu-soal,#menu-ujian,#menu-hasil-ujian').show();
    		} else if (response==='2') {
    			$('#menu-proses-ujian').show();
    		};
    	},
    	error:function(response){
    		console.log(response);
    	}
    });

	$('#btn-simpan-guru').click(function(){
	    console.log('save');
	    if (localStorage.getItem('nip_guru')) {
	    	var link = 'http://localhost/newcat/index.php/c_guru/update_guru';
	    } else {
	    	var link = "http://localhost/newcat/index.php/c_guru/tambah_guru";
	    }
	    console.log(link);
	    $.ajax({
	        url:link,
	        type: 'post',
	        dataType: 'json',
	        async:false,
	        data: {
	            nip: $('#nip-guru').val(),
	            nama:$('#nama').val() ,
	            alamat:$('#alamat').val() ,
	            no_hp: $('#no-hp').val(),
	            email:$('#email').val(),
	            mapel:$('#mapel').val()

	        },
	        cache: false,
	        success: function(response, status, error,pesan) {
	           console.log(response,status,error,pesan);
	           if (response==="Sukses") {
	            alert('Sukses');
	            
	           } else {
	            alert ('Gagal');
	           }

	           ajax_list_guru();
	               },
	        error:function(response){
	            console.log(response);
	        }
	    });
	});

	$('#btn-simpan-siswa').click(function(){
	    console.log('save siswa');
	    $.ajax({
	        url: "http://localhost/newcat/index.php/c_siswa/tambah_siswa",
	        type: 'post',
	        dataType: 'json',
	        data: {
	            nama: $('#nama-siswa').val(),
	            kode:$('#kode-siswa').val() ,
	            tgl_lahir:$('#tgl-lahir').val() ,
	            alamat: $('#alamat-siswa').val(),
	            no_hp:$('#no-hp-siswa').val(),
	            jk:$('input[type=radio][name=jk]').val(),
	            jurusan : $('#jurusan').val()

	        },
	        cache: false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           if (response==='Sukses') {
	            alert('Sukses');
	           } else {
	            alert ('Gagal');
	           }
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            if (response['responseText'].includes('PRIMARY')) {
	                alert('Data Kode Siswa Sudah Ada');
	            };
	        }
	    });
	});
	    
	$('#simpan-mapel').click(function(){
	    console.log('save siswa');
	    $.ajax({
	        url: "http://localhost/newcat/index.php/c_mapel/tambah_mapel",
	        type: 'post',
	        dataType: 'json',
	        data: {
	            nama: $('#nama-mapel').val()
	        },
	        cache: false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           if (response===1) {
	            alert('Sukses');
	           } else {
	            alert ('Gagal');
	           }
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            if (response['responseText'].includes('PRIMARY')) {
	                alert('Data Kode Siswa Sudah Ada');
	            };
	        }
	    });
	});

	    
	function ajax_list_guru(){
		//tabel_guru.clear().draw()
		$.ajax({
	        url: "http://localhost/newcat/index.php/c_guru/list_guru",
	        dataType: 'json',
	        type:'post',
	        cache: false,
	        async:false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           var i = 1;
	           var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-guru'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
	           $.each(response, function() {
	                tabel_guru.row.add([
	                	i,
	                    this['nama_guru'],
	                    this['nip_guru'],
	                    aksi
	                    
	                ]).draw(false);
	                i++;
	            });
	           
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}
    
    function ajax_list_siswa(){
		//tabel_guru.clear().draw()
		$.ajax({
	        url: "http://localhost/newcat/index.php/c_siswa/list_siswa",
	        dataType: 'json',
	        type:'post',
	        cache: false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           var i = 1;
	           var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
	           $.each(response, function() {
	                tabel_siswa.row.add([
	                	i,
	                    this['nama_siswa'],
	                    this['kd_siswa'],
	                    aksi
	                    
	                ]).draw(false);
	                i++;
	            });
	           
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}
	function ajax_list_mapel(){
		//tabel_guru.clear().draw()
		$.ajax({
	        url: "http://localhost/newcat/index.php/c_mapel/list_mapel",
	        dataType: 'json',
	        type:'post',
	        cache: false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           var i = 1;
	           var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
	           $.each(response, function() {
	                tabel_mapel.row.add([
	                	i,
	                    this['nama_pelajaran'],
	                    aksi
	                ]).draw(false);
	                i++;
	            });
	           
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}

	function ajax_param_mapel(id){
		//tabel_guru.clear().draw()
		$.ajax({
	        url: "http://localhost/newcat/index.php/c_mapel/list_mapel",
	        dataType: 'json',
	        type:'post',
	        cache: false,
	        async:false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           	$('<option/>').val('').html('--SILAHKAN PILIH--').appendTo(id).addClass('form-control');
                for (var i = 0; i < response.length; i++) {
                    $('<option/>').val(response[i]['id_pelajaran']).html(response[i]['id_pelajaran'] + ' - ' + response[i]['nama_pelajaran']).appendTo(id);
                }
	           
	        
	        },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}

	function ajax_list_ujian_siswa(){
		//tabel_guru.clear().draw()
		$.ajax({
	        url: "http://localhost/newcat/index.php/c_ujian/list_ujian_siswa",
	        dataType: 'json',
	        type:'post',
	        cache: false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           var i = 1;
	           var x = 0;
	           var aksi = "<a class='green' href='#'><button class='btn btn-sm btn-primary'>Ikuti</button></a>";
	           $.each(response, function() {
	           	console.log(response[x]['status']);
	           	if (response[x]['status']==='Belum Ujian') {
	           		aksi = "<a class='green' href=''><button class='btn btn-sm btn-primary ikuti-ujian'>Ikuti</button></a>";
	           	} else {
	           		aksi2 = "<a class='green' href=''><button class='btn btn-sm btn-primary' disabled>Selesai</button></a>";
	           	}
	                tabel_ujian.row.add([
	                	i,
	                	this['id_pelajaran'],
	                    this['nama_pelajaran'],
	                    this['status'],
	                    aksi
	                ]).draw(false);
	                i++;
	            });
	           
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}

	function ajax_list_soal(){
		//tabel_guru.clear().draw()
		$.ajax({
	        url: "http://localhost/newcat/index.php/c_soal/list_soal",
	        dataType: 'json',
	        type:'post',
	        cache: false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           var i = 1;
	           var aksi = "<a class='green' href=''><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
	           $.each(response, function() {
	                tabel_soal.row.add([
	                	i,
	                    this['soal'],
	                    this['nama_pelajaran'],
	                    this['nama_guru'],
	                    aksi
	                ]).draw(false);
	                i++;
	            });
	           
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}

	function ajax_list_soal_ujian(){
		console.log('list soal ujian');
		//console.log(localStorage.getItem('id_pel'));
		//tabel_guru.clear().draw()
		$.ajax({
	        url: "http://localhost/newcat/index.php/c_soal/list_soal_ujian",
	        dataType: 'json',
	        data:{id_pelajaran:localStorage.getItem('id_ujian')},
	        type:'post',
	        cache: false,
	        async:false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           var i = 1;
	           var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
	           $.each(response, function() {
	                tabel_soal_ujian.row.add([
	                	i,
	                    this['soal']
	                ]).draw(false);
	                i++;
	            });
	           
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}

	function ajax_guru_by_nip(nip){
		//tabel_guru.clear().draw()

		$.ajax({
	        url: "http://localhost/newcat/index.php/c_guru/guru_by_nip",
	        dataType: 'json',
	        type:'post',
	        data:{nip:localStorage.getItem('nip_guru')},
	        cache: false,
	        async:false,
	        success: function(response, status, error,pesan) {
	           console.log(response);
	           
	           $('#nip-guru').prop('disabled',true);
	           $('#nip-guru').val(response['nip_guru']);
	           $('#nama').val(response['nama_guru']);
	           $('#alamat').val(response['alamat']);
	           $('#no-hp').val(response['no_hp']);
	           $('#email').val(response['email']);
	           $('#mapel').val(response['id_mapel']);
	           

	           //var i = 1;
	           //var aksi = "<a class='green' href=''><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
	          /* $.each(response, function() {
	                tabel_soal.row.add([
	                	i,
	                    this['soal'],
	                    this['nama_pelajaran'],
	                    this['nama_guru'],
	                    aksi
	                ]).draw(false);
	                i++;
	            });*/
	           
	               },
	        error:function(response){
	            console.log(response['responseText']);
	            
	        }
	    }); 
	}


	
});








