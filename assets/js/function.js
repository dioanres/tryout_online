//Login

$(document).ready(function() {
    var tabel_guru = $('#tbl-guru').DataTable({
        "responsive": true,
        "columnDefs": [{
            "targets": [1],
            "visible": false,
            "searchable": false
        }],
        "paginate": true,
        "lengthChange": false,
        "info": false,
        "autoWidth": false,
        "displayLength": 10,
        "filter": false,
        "paging": true,
    });
    var tabel_siswa = $('#tbl-siswa').DataTable({
        "responsive": true,
        "filter": true,
        "paging": false,
        "info": false
    });

    var tabel_mapel = $('#tbl-mapel').DataTable({
        "responsive": true,
        "filter": true,
        "paging": false,
        "info": false,
        "columnDefs": [{
            "targets": [1],
            "visible": false,
            "searchable": false
        }],
    });

    var tabel_ujian = $('#tbl-ujian').DataTable({
        "responsive": true,
        "filter": true,
        "paging": false,
        "info": false,
        "columnDefs": [{
            "targets": [1],
            "className": "hide"
        }]
    });

    var tabel_ujians = $('#tbl-ujians').DataTable({
        "responsive": true,
        "filter": true,
        "paging": false,
        "info": false,
        "columnDefs": [{
            "targets": [1],
            "className": "hide"
        }]
    });


    /*var tabel_soal = $('#tbl-soal').DataTable({
        "responsive": true,
        "filter": true,
        "paging": false,
        "info": false,
        
    });*/

    var tabel_soal_ujian = $('#tbl-soal-ujian').DataTable({
        "responsive": true,
        "filter": false,
        "paging": false,
        "info": false
    });


    var last_cat = window.location.href.split("/").pop();
    console.log(last_cat);
    if (last_cat === 'c_guru' || last_cat === 'c_guru#') {
        ajax_list_guru();
    } else if (last_cat === 'c_siswa' || last_cat === 'c_siswa#') {
        ajax_list_siswa();
    } else if (last_cat === 'c_mapel' || last_cat === 'c_mapel#') {
        ajax_list_mapel();
    } else if (last_cat === 'proses_ujian') {
        ajax_list_ujian_siswa();
    } else if (last_cat === 'tambah_soal') {
        ajax_param_mapel('#slc-pelajaran-soal');
    } else if (last_cat === 'c_soal' || last_cat === 'c_soal#') {
        ajax_list_soal();
    } else if (last_cat === 'tampil_soal_ujian') {
        ajax_list_soal_ujian();
    } else if (last_cat === 'c_ujian' || last_cat === 'c_ujian#') {
        ajax_list_ujian();
    };

    var arr_soal = [];
    $('#tbl-ujian tbody').on('click', '.ikuti-ujian', function() {


        $(this).addClass('selected');
        if (typeof tabel_ujian.row($(this).closest('tr')).data() !== 'undefined') {
            arr_soal = tabel_ujian.row($(this).closest('tr')).data();
        } else if (typeof tabel_ujian.row(this).data() !== 'undefined') {
            arr_soal = tabel_ujian.row(this).data();
        } else {
            arr_soal = tabel_ujian.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }
        console.log(arr_soal);
        localStorage.setItem('id_ujian', arr_soal[5]);

        location.href = base_url+ 'index.php/c_ujian/tampil_soal_ujian';
        ajax_list_soal_ujian();

        console.log(localStorage.getItem('id_ujian'));

    });

    var arr_guru = [];
    $('#tbl-guru tbody').on('click', '.dtl-guru , .delete-guru', function() {
        ajax_param_mapel('#mapel');
        $(this).addClass('selected');
        if (typeof tabel_guru.row($(this).closest('tr')).data() !== 'undefined') {
            arr_guru = tabel_guru.row($(this).closest('tr')).data();
        } else if (typeof tabel_guru.row(this).data() !== 'undefined') {
            arr_guru = tabel_guru.row(this).data();
        } else {
            arr_guru = tabel_guru.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }


        //localStorage.setItem('nip_guru', arr_guru[2]);
        if ($(this).attr('class').includes('dtl-guru')) {
            $('#mod-guru').modal('show');
            ajax_guru_by_nip(arr_guru[3]);
        } else {
            $.confirm({
                title: 'Konfirmasi',
                content: 'Yakin Ingin Hapus Data?',
                buttons: {
                    confirm: function() {
                        ajax_delete(arr_guru[1], base_url+ "index.php/c_guru/delete_guru");
                        //$.alert('Data Berhasil dihapus!');
                        ajax_list_guru();
                    },
                    cancel: function() {
                        //$.alert('Canceled!');
                        ajax_list_guru();
                    }
                }
            });

        }

    });

    var arr_siswa = [];
    var flag_edit = 0;
    $('#tbl-siswa tbody').on('click', '.dtl-siswa, .delete-siswa', function() {
        $('#kode-siswa').prop('disabled', true);
        //$('#mod-siswa').modal('show');
        $(this).addClass('selected');
        if (typeof tabel_siswa.row($(this).closest('tr')).data() !== 'undefined') {
            arr_siswa = tabel_siswa.row($(this).closest('tr')).data();
        } else if (typeof tabel_siswa.row(this).data() !== 'undefined') {
            arr_siswa = tabel_siswa.row(this).data();
        } else {
            arr_siswa = tabel_siswa.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }


        //localStorage.setItem('kd_siswa', arr_siswa[2]);
        if ($(this).attr('class').includes('dtl-siswa')) {
            $('#mod-siswa').modal('show');
            ajax_param_mapel('#mapel-siswa');
            ajax_siswa_by_id(arr_siswa[2]);
            
        } else {
            $.confirm({
                title: 'Konfirmasi',
                content: 'Yakin Ingin Hapus Data?',
                buttons: {
                    confirm: function() {
                        ajax_delete(arr_siswa[2], base_url+ "index.php/c_siswa/delete_siswa");
                        //$.alert('Data Berhasil dihapus!');
                        ajax_list_siswa();
                    },
                    cancel: function() {
                        //$.alert('Canceled!');
                        //ajax_list_guru();
                    }
                }
            });

        }

    });

    var arr_mapel = [];
    $('#tbl-mapel tbody').on('click', '.dtl-mapel, .delete-mapel', function() {
        //$('#kode-siswa').prop('disabled',true);
        //$('#mod-mapel').modal('show');
        $(this).addClass('selected');
        if (typeof tabel_mapel.row($(this).closest('tr')).data() !== 'undefined') {
            arr_mapel = tabel_mapel.row($(this).closest('tr')).data();
        } else if (typeof tabel_mapel.row(this).data() !== 'undefined') {
            arr_mapel = tabel_mapel.row(this).data();
        } else {
            arr_mapel = tabel_mapel.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }
        if ($(this).attr('class').includes('dtl-mapel')) {
            $('#mod-mapel').modal('show');
            ajax_mapel_by_id(arr_mapel[1]);
        } else {
            $.confirm({
                title: 'Konfirmasi',
                content: 'Yakin Ingin Hapus Data?',
                buttons: {
                    confirm: function() {
                        ajax_delete(arr_mapel[1], base_url+ "index.php/c_mapel/delete_mapel");
                        //$.alert('Data Berhasil dihapus!');
                        ajax_list_mapel();
                    },
                    cancel: function() {
                        //$.alert('Canceled!');
                        //ajax_list_guru();
                    }
                }
            });

        }


    });

    var arr_ujian = [];
    $('#tbl-ujians tbody').on('click', '.dtl-ujian, .delete-ujian', function() {
        //$('#kode-siswa').prop('disabled',true);
        //$('#mod-mapel').modal('show');
        ajax_param_mapel('#mapel-ujian');
        $(this).addClass('selected');
        if (typeof tabel_ujians.row($(this).closest('tr')).data() !== 'undefined') {
            arr_ujian = tabel_ujians.row($(this).closest('tr')).data();
        } else if (typeof tabel_ujians.row(this).data() !== 'undefined') {
            arr_ujian = tabel_ujians.row(this).data();
        } else {
            arr_ujian = tabel_ujians.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }
        if ($(this).attr('class').includes('dtl-ujian')) {
            $('#mod-ujian').modal('show');
            ajax_ujian_by_id(arr_ujian[1]);
        } else {
            $.confirm({
                title: 'Konfirmasi',
                content: 'Yakin Ingin Hapus Data?',
                buttons: {
                    confirm: function() {
                        ajax_delete(arr_ujian[1], base_url+ "index.php/c_ujian/delete_ujian");
                        //$.alert('Data Berhasil dihapus!');
                        ajax_list_ujian();
                    },
                    cancel: function() {
                        //$.alert('Canceled!');
                        //ajax_list_guru();
                    }
                }
            });

        }


    });


    $('#menu-guru').click(function() {
        //alert('guru');
        console.log('list guru');
        ajax_list_guru();
    });

    $('#menu-siswa').click(function() {
        ajax_list_siswa();
    });

    $('#menu-mapel').click(function() {
        ajax_list_mapel();
    });

    $('#btn-tambah-siswa').click(function() {
        ajax_param_mapel('#mapel-siswa');
        localStorage.setItem('kd_siswa', '');
    });

    $('#tambah-guru').click(function() {
        $('#nip-guru').prop('disabled', false);
        $('.mdl-tambah-guru').find("input[type=text], textarea,select").val("");

        ajax_param_mapel('#mapel');
    });

    $('#btn-tambah-mapel').click(function() {
        $('.mdl-tambah-mapel').find("input[type=text], textarea,select").val("");
    });

    $('#tambah-ujian').click(function() {

        $('.mdl-tambah-ujian').find("input[type=text],input[type=number], textarea,select").val("");
        ajax_param_mapel('#mapel-ujian');
    });

    $('#btn-tambah-soal').click(function() {
        //localStorage.setItem('id_ujian',);
        ajax_param_mapel('#slc-pelajaran-soal');
    });

    $('#menu-proses-ujian').click(function() {
        ajax_list_ujian_siswa();
    });

    $('#menu-soal').click(function() {
        ajax_list_soal();
    });

    $('#btn-tambah-siswa').click(function() {
        $('#kode-siswa').prop('disabled', false);
        $('.mdl-tambah-siswa').find("input[type=text], radio,textarea,select").val("");
    });

    $('#log-out').click(function() {
        clear_localstorage();
        id_guru = '';
    });

    var id_guru = '';
    $.ajax({
        url: base_url+"index.php/c_login/akses",
        type: 'post',
        cache: false,
        success: function(response) {
            
            localStorage.setItem('nama_login', response['nama']);
            $('#nm_login').html(response['nama']);
            if (response['role'] === '1') {
                $('#menu-guru').show();
                $('#menu-siswa').show();
                $('#menu-mapel').show();
            } else if (response['role'] === '2') {
                $('#menu-soal,#menu-ujian,#menu-hasil-ujian,#report-ujian').show();
            } else if (response['role'] === '3') {
                $('#menu-proses-ujian').show();
            };
        },
        error: function(response) {
            console.log(response);
        }
    });

    $('#btn-simpan-guru').click(function() {
        console.log('save');
        if (arr_guru[2]) {
            var link = base_url+ 'index.php/c_guru/update_guru';
        } else {
            var link = base_url+ "index.php/c_guru/tambah_guru";
        }
        console.log(link);
        $.ajax({
            url: link,
            type: 'post',
            dataType: 'json',
            async: false,
            data: {
                nip: $('#nip-guru').val(),
                nama: $('#nama').val(),
                alamat: $('#alamat').val(),
                no_hp: $('#no-hp').val(),
                email: $('#email').val(),
                mapel: $('#mapel').val()

            },
            cache: false,
            success: function(response, status, error, pesan) {
                console.log(response, status, error, pesan);
                if (response === 1) {
                    alert_info('Info', 'Data Berhasil Disimpan');
                    ajax_list_guru();
                } else {
                    alert_info('Error', response);
                }
            },
            error: function(response) {
                console.log(response);
                alert_info('Error', response['responseText']);
            }
        });
    });

    $('#btn-simpan-siswa').click(function() {
        console.log('save siswa');
        console.log(arr_siswa[2]);
        var kd_siswa = arr_siswa[2];
        if (kd_siswa) {
            var link = base_url+ "index.php/c_siswa/update_siswa";
        } else {
            var link = base_url+ "index.php/C_siswa/tambah_siswa";
        };
        console.log(link);
        $.ajax({
            url: link,
            type: 'post',
            dataType: 'json',
            data: {
                nama: $('#nama-siswa').val(),
                kode: $('#kode-siswa').val(),
                tgl_lahir: $('#tgl-lahir').val(),
                alamat: $('#alamat-siswa').val(),
                no_hp: $('#no-hp-siswa').val(),
                jk: $('input[type=radio][name=jk]').val(),
                jurusan: $('#jurusan').val(),
                mapel:$('#mapel-siswa').val()

            },
            cache: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                if (response === 1) {
                    alert_info('Info', 'Data Berhasil Disimpan');
                    ajax_list_siswa();
                } else {
                    alert_info('Error', response);
                }
            },
            error: function(response) {
                
                if (response['responseText'].includes('PRIMARY')) {
                    alert_info('Error', 'Data Kode Siswa Sudah Ada');
                };

            }
        });
    });

    $('#simpan-mapel').click(function() {
        var id_pel = arr_mapel[1];
        if (id_pel) {
            var link = base_url+ "index.php/c_mapel/update_mapel";
        } else {
            var link = base_url+ "index.php/c_mapel/tambah_mapel";
        }
        console.log('save siswa');
        $.ajax({
            url: link,
            type: 'post',
            dataType: 'json',
            data: {
                nama: $('#nama-mapel').val(),
                id_pel: id_pel
            },
            cache: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                if (response === 1) {
                    alert_info('Info', 'Data Berhasil Disimpan');
                    ajax_list_mapel();
                } else {
                    alert_info('Error', response);
                }
            },
            error: function(response) {
                console.log(response);
                console.log(response['responseText']);
                if (response['responseText'].includes('PRIMARY')) {
                    alert('Data Kode Siswa Sudah Ada');
                };
            }
        });
    });

    $('#simpan-mapel-ujian').click(function() {
        if (arr_ujian[1]) {
            var link = base_url+ "index.php/c_ujian/update_ujian";
        } else {
            var link = base_url+ "index.php/c_ujian/tambah_ujian";
        }
        
        $.ajax({
            url: link,
            type: 'post',
            dataType: 'json',
            async: false,
            data: {
                id: arr_ujian[1],
                nama_ujian: $('#nama-ujian').val(),
                mapel_ujian: $('#mapel-ujian').val(),
                waktu: $('#waktu').val()
            },
            cache: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                if (response === 1) {
                    alert_info('Info', 'Data Berhasil Disimpan');
                    ajax_list_ujian();
                } else {
                    alert_info('Error', response);
                }
            },
            error: function(response) {
                console.log(response['responseText']);

                alert_info('Error', response['responseText']);

            }
        });
    });

    function ajax_list_guru() {
        tabel_guru.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_guru/list_guru",
            dataType: 'json',
            type: 'post',
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                var i = 1;
                var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-guru'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-guru'></i></a>";
                $.each(response, function() {
                    tabel_guru.row.add([
                        i,
                        this['id_guru'],
                        this['nama_guru'],
                        this['nip_guru'],
                        aksi

                    ]).draw(false);
                    i++;
                });

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function ajax_list_siswa() {
        tabel_siswa.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_siswa/list_siswa",
            dataType: 'json',
            type: 'post',
            cache: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                var i = 1;
                var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-siswa'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-siswa'></i></a>";
                $.each(response, function() {
                    tabel_siswa.row.add([
                        i,
                        this['nama_siswa'],
                        this['kd_siswa'],
                        this['mapel'] ? get_desc_mapel_by_id(this['mapel']) : '',
                        aksi

                    ]).draw(false);
                    i++;
                });

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function get_desc_mapel_by_id(id) {
        var a = '';
        $.ajax({
            url:base_url+ "index.php/c_siswa/list_mapel_by_id",
            data:{id:id},
            dataType:'json',
            type:'post',
            async:false,
            success:function(response) {
                console.log(response);
                a = response.toString();
                console.log(a);
                return 'test';
            }
        });

        return a;
    }

    function ajax_list_mapel() {
        tabel_mapel.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_mapel/list_mapel",
            dataType: 'json',
            type: 'post',
            cache: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                var i = 1;
                var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2 dtl-mapel'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2 delete-mapel'></i></a>";
                $.each(response, function() {
                    tabel_mapel.row.add([
                        i,
                        this['id_pelajaran'],
                        this['nama_pelajaran'],
                        aksi
                    ]).draw(false);
                    i++;
                });

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function ajax_param_mapel(id) {
        $(id).find('option').remove();
        $.ajax({
            url: base_url+ "index.php/c_mapel/list_mapel",
            dataType: 'json',
            type: 'post',
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                
                for (var i = 0; i < response.length; i++) {
                    $('<option/>').val(response[i]['id_pelajaran']).html(response[i]['id_pelajaran'] + ' - ' + response[i]['nama_pelajaran']).appendTo(id);
                }


            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    //edit rn
    function ajax_list_ujian_siswa() {
        //tabel_guru.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_ujian/list_ujian_siswa",
            dataType: 'json',
            type: 'post',
            cache: false,
            success: function(response, status, error, pesan) {
                
                var i = 1;
                var aksi = "<a class='green' href='#'><button class='btn btn-sm btn-primary'>Ikuti</button></a>";
                $.each(response, function(x) {
                    if (response[x]['status'] === 'Belum Ujian') {
                        aksi = "<a class='green' href='"+base_url+"index.php/c_ujian/tampil_soal_ujian'><button class='btn btn-sm btn-primary ikuti-ujian'>Ikuti</button></a>";
                    } else {
                        aksi = "<span class='green'><button class='btn btn-sm btn-primary btn-lihat-nilai'>Lihat Nilai</button></span>";
                    }

                    tabel_ujian.row.add([
                        i,
                        this['id_pelajaran'],
                        this['ujian'],
                        this['status'],
                        aksi,
                        this['id_ujian'],
                        this['score']
                    ]).draw(false);
                    i++;
                });

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    $('#tbl-ujian tbody').on('mouseover', '.btn-lihat-nilai', function() {
        console.log(arr_soal);
        if (typeof tabel_ujian.row($(this).closest('tr')).data() !== 'undefined') {
            arr_soal = tabel_ujian.row($(this).closest('tr')).data();
        } else if (typeof tabel_ujian.row(this).data() !== 'undefined') {
            arr_soal = tabel_ujian.row(this).data();
        } else {
            arr_soal = tabel_ujian.row($(this).parent().parent().parent().parent().parent().prev()).data();
        }

        $(this).html("SCORE = <strong>" + arr_soal[6] + "</strong>");
    });

    $('#tbl-ujian tbody').on('mouseout', '.btn-lihat-nilai', function() {
        $(this).html("Lihat Nilai");
    });

    function ajax_list_soal() {
        //tabel_guru.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_soal/list_soal",
            dataType: 'json',
            type: 'post',
            cache: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                var i = 1;
                var aksi = "<a class='green' href=''><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
                $.each(response, function() {
                    tabel_soal.row.add([
                        i,
                        this['soal'],
                        this['nama_pelajaran'],
                        this['nama_guru'],
                        aksi
                    ]).draw(false);
                    i++;
                });

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function ajax_list_ujian() {
        tabel_ujians.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_ujian/list_ujian",
            dataType: 'json',
            type: 'post',
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                var i = 1;
                $.each(response, function() {
                    var aksi = "<a class='green' href='"+base_url+"index.php/c_soal/tambah_soal/" + this['id_ujian'] + "'><i class='ace-icon fa fa-plus bigger-130 col-sm-2'></i></a><a class='green' href='"+base_url+"index.php/c_soal/update_soal/" + this['id_ujian'] + "'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a>";
                    tabel_ujians.row.add([
                        i,
                        this['id_ujian'],
                        this['nama_ujian'],
                        this['nama_pelajaran'],
                        this['waktu'],
                        aksi
                    ]).draw(false);
                    i++;
                });

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    //edit rn
    function ajax_list_soal_ujian() {
        //tabel_guru.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_soal/list_soal_ujian",
            dataType: 'json',
            data: {
                id_ujian: localStorage.getItem('id_ujian')
            },
            type: 'post',
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                soal = response;
                var aksi = "<a class='green' href='#'><i class='ace-icon fa fa-pencil bigger-130 col-sm-2'></i></a><a class='red' href='#'><i class='ace-icon fa fa-trash-o bigger-130 col-sm-2'></i></a>";
                //setTimeout(selesai_ujian, response[0]['waktu'] * 1000 * 60);
                CountDown(response[0]['waktu'] * 60, $('#countdown'));
                $('#div-ujian-nama').html(response[0]['nama_ujian']);
                $('#div-mapel').html(response[0]['nama_pelajaran']);
                $.each(response, function(i) {
                    /*tabel_soal_ujian.row.add([
                    	i,
                        this['soal']
                    ]).draw(false);
                    i++;*/
                    $('#div-ujian').append('<div id = "soal-' + this['id_soal'] + '"></div>')
                    $('#soal-' + this['id_soal']).append(i + 1 + '. ' + this['soal']);
                    $('#soal-' + this['id_soal']).append('<p class = "jawaban"> <label><input value="A" type="radio" name="jwb-' + this['id_soal'] + '"> ' + this['pil_a'] + '</p></label>');
                    $('#soal-' + this['id_soal']).append('<p class = "jawaban"> <label><input value="B" type="radio" name="jwb-' + this['id_soal'] + '"> ' + this['pil_b'] + '</p></label>');
                    $('#soal-' + this['id_soal']).append('<p class = "jawaban"> <label><input value="C" type="radio" name="jwb-' + this['id_soal'] + '"> ' + this['pil_c'] + '</p></label>');
                    $('#soal-' + this['id_soal']).append('<p class = "jawaban"> <label><input value="D" type="radio" name="jwb-' + this['id_soal'] + '"> ' + this['pil_d'] + '</p></label>');
                    $('#soal-' + this['id_soal']).css('margin-bottom', '10px');
                });
                $('.jawaban').css('margin-bottom', '0px');
                $('.jawaban').css('margin-left', '20px');
            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function waktu_ujian() {
        /*$('#mod-timeout').modal('show');
        console.log('tampil modal show');*/
    }

    function ajax_guru_by_nip(nip) {
        //tabel_guru.clear().draw()
        console.log(nip);
        $.ajax({
            url: base_url+ "index.php/c_guru/guru_by_nip",
            dataType: 'json',
            type: 'post',
            data: { nip: nip },
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                console.log(response);

                $('#nip-guru').prop('disabled', true);
                $('#nip-guru').val(response['nip_guru']);
                $('#nama').val(response['nama_guru']);
                $('#alamat').val(response['alamat']);
                $('#no-hp').val(response['no_hp']);
                $('#email').val(response['email']);
                $('#mapel').val(response['id_mapel']);

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function ajax_siswa_by_id(kd_siswa) {

        $.ajax({
            url: base_url+ "index.php/c_siswa/siswa_by_id",
            dataType: 'json',
            type: 'post',
            data: { kd_siswa: kd_siswa },
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                console.log(response);

                $('#kode-siswa').prop('disabled', true);
                $('#nama-siswa').val(response['nama_siswa']);
                $('#kode-siswa').val(response['kd_siswa']);
                $('#tgl-lahir').val(response['tgl_lahir_siswa']);
                $('#alamat-siswa').val(response['alamat_siswa']);
                $('#no-hp-siswa').val(response['no_hp']);
                $('#jurusan').val(response['id_mapel']);
                if(response['mapel']){
                    $.each(response['mapel'], function(i,v){
                        $('#mapel-siswa  option[value="'+v+'"]').prop("selected", true);
                    });
                }
                
                if (response['jenis_kelamin'] === 'L') {
                    $('#jk-l').prop('checked', true);
                    $('#jk-p').prop('checked', false);
                } else if (response['jenis_kelamin'] === 'P') {
                    $('#jk-l').prop('checked', false);
                    $('#jk-p').prop('checked', true);
                };

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function ajax_mapel_by_id(id) {
        //tabel_guru.clear().draw()

        $.ajax({
            url: base_url+ "index.php/c_mapel/mapel_by_nip",
            dataType: 'json',
            type: 'post',
            data: { id: id },
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                console.log(response);

                //$('#nama-mapel').prop('disabled',true);
                $('#nama-mapel').val(response['nama_pelajaran']);

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }

    function ajax_ujian_by_id(id) {
        //tabel_guru.clear().draw()
        $.ajax({
            url: base_url+ "index.php/c_ujian/ujian_by_id",
            dataType: 'json',
            type: 'post',
            data: { id: id },
            cache: false,
            async: false,
            success: function(response, status, error, pesan) {
                console.log(response);
                $('#nama-ujian').val(response['nama_ujian']);
                $('#mapel-ujian').val(response['id_pelajaran']);
                $('#waktu').val(response['waktu']);

            },
            error: function(response) {
                console.log(response['responseText']);

            }
        });
    }



    function clear_localstorage() {
        localStorage.setItem('id_ujian', '');
        localStorage.setItem('id_siswa', '');
        localStorage.setItem('kd_siswa', '');
        localStorage.setItem('nip_guru', '');
        localStorage.setItem('id_pel', '');
    }
});

function ajax_delete(id, url) {
    //tabel_guru.clear().draw()
    console.log(id);
    $.ajax({
        url: url,
        dataType: 'json',
        type: 'post',
        data: { id: id },
        cache: false,
        async: false,
        success: function(response, status, error, pesan) {
            console.log(response);
            $.alert('Data Berhasil dihapus!');
        },
        error: function(response) {
            console.log(response['responseText']);
            alert_info('Error', response['responseText']);

        }
    });
}

function alert_confirm(pesan) {
    $.confirm({
        title: 'Konfirmasi',
        content: pesan,
        buttons: {
            confirm: function() {
                $.alert('Data Berhasil dihapus');
            },
            cancel: function() {
                $.alert('Canceled!');
            }
        }
    });
}

function alert_info(title, pesan) {
    $.alert({
        title: title,
        content: pesan,
    });
}