<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
// if ( ! function_exists('random')){
//    function random(){
//          $number = rand(1111,9999);
//          return $number;
//        }
//    }
 
if ( ! function_exists('role')){
   function role(){
        $role = [
            "admin" => 1,
            "guru" => 2,
            "siswa" => 3
        ];
        return $role;
       }
   }

if ( ! function_exists('date_now')){
function date_now(){

        $date_now = date("Y-m-d H:i:s");

        return $date_now;
    }
}