<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';

class model_mapel extends CI_Model {

	public function list_mapel(){
		$result = $this->db->get('t_mapel')->result();
        
        return $result;
		
    }
    
    public function list_mapel_by_id($id)
    {
        //$id = json_encode($id);
        $this->db->where_in('id_pelajaran',$id);
        $result = $this->db->get('t_mapel')->result();
        $new_result = [];

        foreach ($result as $key => $value) {
            array_push($new_result,$value->nama_pelajaran);
        }

        return $new_result;
    }

	
}
