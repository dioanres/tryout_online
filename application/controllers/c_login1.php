<?php 

session_start();
class C_login extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		//$this->load->model('models');
		$this->load->library('session');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function login()
	{
		/*var_dump("masuk");
		$this->load->view('v_guru');
			//echo "masuk";
			exit();*/
		$this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|callback_username_check');
		$this->form_validation->set_rules('password', 'Password');
		//var_dump($_SESSION['logged_in_acq_menu']);
		//exit();
		if($this->form_validation->run() == FALSE)
			{
				$this->load->view('header');
				$this->load->view('login');
				$this->load->view('footer');
			}
		else
		if($this->session->userdata('akses'))
		{
			//exit();
			redirect(site_url('c_home'), 'refresh');
		}
		else
		{
			redirect(site_url('c_home'), 'refresh');
		}
	}

	public function username_check($user)
	{
		$pass                = $this->input->post('password');
		if($user == '' && $pass == '')
		{
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Username dan Password Harus Diisi</div>');
			return FALSE;
		}
		elseif($user == '' || $user == null)
		{
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Username harus diisi</div>');
			return FALSE;
		}
		elseif ($pass == '' || $pass == null) {
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Password harus diisi</div>');
			return FALSE;
		}
		else
		{		
			$a=$this->db->query("select akses,kd_admin,nama_admin from t_admin where kd_admin='$pass' and nama_admin='$user' ")->row();
			//$_SESSION['akses_cat'] = $a->akses;
			/*var_dump($a);
			//echo $a;
			exit();*/
			$this->session->set_userdata('akses_login', $a);
			
			$sess = $this->session->userdata('akses');
			
			//$this->load->view('side_bar',$sess);
			
		}
	}

	public function logout() {
		$this->session->unset_userdata('akses');
		$this->session->sess_destroy();
		redirect(site_url('C_login'), 'refresh');
	}

	public function akses() {
		$data = $this->session->userdata('akses_login')->akses;
		/*var_dump($data);
			//echo $a;
			exit();*/
		$this->output->set_content_type('application/text');
        $this->output->set_output($data);
	}
}
