<?php 
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';

class C_home extends CI_Controller {

	
	public function __construct()
	{
		parent:: __construct();
		

		
	}
	public function index()
	{

		$jml_guru = $this->db->count_all('t_guru');
		$jml_siswa = $this->db->count_all('t_siswa');
		$jml_ujian = $this->db->count_all('t_ujian');
		
		$info = array(
				'guru' => $jml_guru,
				'siswa' => $jml_siswa,
				'ujian' => $jml_ujian,
				'akses_login' => $this->session->userdata('akses_login')
			);

		$this->load->view('v_home',$info);
		
	}
}