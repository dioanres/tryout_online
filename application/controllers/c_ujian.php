<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';

class C_ujian extends CI_Controller {

	
	public function index()
	{
		$this->load->view('v_ujian');
	}
	public function hasil_ujian()
	{
		$this->load->view('v_hasil_ujian');
	}

	public function proses_ujian()
	{
		$this->load->view('v_proses_ujian');
	}

	public function view_hasil_ujian_siswa()
	{
		$this->load->view('v_hasil_ujian_siswa');
	}

	public function tampil_soal_ujian()
	{
		$this->load->view('v_soal_ujian');
		$this->load->view('modal_timeout');
	}

	public function list_hasil_ujian_detail_siswa()
	{
		$this->load->view('v_soal_ujian');
	}

	public function tambah_ujian() {
		
		$nama = $this->input->post('nama_ujian');
		$mapel = $this->input->post('mapel_ujian');
		$waktu = $this->input->post('waktu');
		$guru = $this->session->userdata('akses_login')->username;
		
		$ret = $this->db->query("insert into t_ujian (nama_ujian,id_guru,id_pelajaran,waktu) values ('$nama','$guru','$mapel','$waktu')");
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function update_ujian() {
		$id=$this->input->post('id');
		$nama = $this->input->post('nama_ujian');
		$mapel = $this->input->post('mapel_ujian');
		$waktu = $this->input->post('waktu');
		
		$ret = $this->db->query("UPDATE t_ujian set nama_ujian='$nama',id_pelajaran='$mapel',waktu=$waktu WHERE id_ujian=$id");
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function delete_ujian() {
		$id = $this->input->post('id');
		$q = "delete from t_ujian WHERE id_ujian=$id";
		/*var_dump($q);
		//var_dump($nip);
		exit();*/
		$ret = $this->db->query($q);
		
		if ($ret == 1) {
			$data ="Sukses";
		} else {
			$data = "Gagal";
		}
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function list_ujian(){
		$nip = $this->session->userdata('akses_login')->username;
		
		$result = $this->db->query("select * from t_ujian a, t_mapel b, t_guru c WHERE a.id_pelajaran=b.id_pelajaran AND a.id_guru=c.nip_guru
and c.nip_guru=$nip")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function list_hasil_ujian(){
		$id = $this->session->userdata('akses_login')->kd_admin;
		$result = $this->db->query("SELECT u.id_ujian, u.nama_ujian, m.nama_pelajaran, u.waktu, COUNT(s.id_soal) as jumlah FROM t_ujian u LEFT join t_soal s on u.id_ujian = s.id_ujian LEFT JOIN t_mapel m on u.id_pelajaran
 = m.id_pelajaran where u.id_guru = '$id' GROUP BY u.nama_ujian, m.nama_pelajaran, u.waktu ")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function list_hasil_ujian_detail(){
		$id_ujian = $this->input->post('id_ujian');
		$result = $this->db->query("SELECT a.nama_admin, h.score, a.id_admin,case when h.score < 55 THEN 'Tidak Lulus' when h.score > 55 then 'Lulus' End Keterangan FROM t_hasil_ujian_head h left join t_admin a on h.id_siswa = a.kd_admin WHERE h.id_ujian = '$id_ujian'")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function list_ujian_siswa(){
		$id = $this->session->userdata('akses_login')->username;

// 		$result = $this->db->query("select c.score, a.id_pelajaran, b.id_ujian,CONCAT(a.nama_pelajaran, ' - ', b.nama_ujian) AS ujian,case when c.status='0' then 'Belum Ujian' when c.status = '1' then 'Sudah Ujian' else 'Belum Ujian' end AS status 
// from t_ujian b LEFT JOIN t_mapel a ON a.id_pelajaran = b.id_pelajaran 
// LEFT JOIN t_hasil_ujian_head c ON b.id_ujian = c.id_ujian and c.id_siswa='$id'")->result();

		$result = $this->db->query("select d.id_siswa,c.score, a.id_pelajaran, b.id_ujian,CONCAT(a.nama_pelajaran, ' - ', b.nama_ujian) AS ujian,case when c.status='0' then 'Belum Ujian' when c.status = '1' then 'Sudah Ujian' else 'Belum Ujian' end AS status 
		from t_ujian b LEFT JOIN t_mapel a ON a.id_pelajaran = b.id_pelajaran 
		LEFT JOIN t_hasil_ujian_head c ON b.id_ujian = c.id_ujian 
		JOIN siswa_has_mapel d on d.id_mapel = b.id_pelajaran
		and d.id_siswa='$id'")->result();
		
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
    }

    public function ujian_by_id(){
		$id = $this->input->post('id');
		$result = $this->db->query("select * from t_ujian where id_ujian = '$id'")->row();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function list_soal_ujian_siswa(){
		$id_ujian = $this->input->post('id_ujian');
		$result = $this->db->query("select s.id_soal, m.nama_pelajaran, u.nama_ujian, s.soal, s.pil_a, s.pil_b, s.pil_c, s.pil_d, s.jawaban, hu.jawaban as 'jwbn' from t_ujian u LEFT JOIN t_soal s on u.id_ujian = s.id_ujian left join t_hasil_ujian hu on s.id_soal = hu.id_soal LEFT join t_mapel m on u.id_pelajaran = m.id_pelajaran  where u.id_ujian = '$id_ujian' ORDER BY rand()")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}
}
