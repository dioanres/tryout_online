<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require __DIR__ . '/../../vendor/autoload.php';

class C_mapel extends CI_Controller
{

    public function index()
    {
        $this->load->view('v_mapel');
        $this->load->view('modal_mapel');
    }

    public function tambah_mapel()
    {

        $nama = $this->input->post('nama');

        $ret = $this->db->query("insert into t_mapel (nama_pelajaran) values ('$nama')");
        $this->output->set_content_type('application/text');
        $this->output->set_output($ret);
    }

    public function update_mapel()
    {

        $nama = $this->input->post('nama');
        $id = $this->input->post('id_pel');

        $q = "UPDATE t_mapel SET nama_pelajaran='$nama' where id_pelajaran=$id";
        $ret = $this->db->query($q);
        $this->output->set_content_type('application/text');
        $this->output->set_output($ret);
    }

    public function delete_mapel()
    {
        $id = $this->input->post('id');
        $q = "delete from t_mapel WHERE id_pelajaran=$id";
        $ret = $this->db->query($q);
        $this->output->set_content_type('application/text');
        $this->output->set_output($ret);
    }

    public function list_mapel_by_guru()
    {
        $result = $this->db->query("select * from t_mapel where id_pelajaran = '" . $this->session->userdata('akses_login')->id_mapel . "'")->result();

        return $result;
    }

    public function list_mapel()
    {

        if ($this->session->userdata('akses_login')->role == role()['admin']) {
            $result = $this->db->query("select * from t_mapel")->result();
        } else {
            $result = $this->list_mapel_by_guru();
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
    }

    public function mapel_by_nip()
    {
        $id = $this->input->post('id');
        $result = $this->db->query("select * from t_mapel where id_pelajaran = " . $id . "")->row();

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
    }
}
