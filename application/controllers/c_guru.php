<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';

class C_guru extends CI_Controller {


	public function __construct()
	{
		parent:: __construct();
		//$this->load->model('models');
		$this->load->library('session');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
	}

	public function index()
	{	

		$this->load->view('v_guru');
		$this->load->view('modal_guru');
	}

	public function tambah_guru() {
		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$email = $this->input->post('email');
		$mapel = $this->input->post('mapel');
		$role = role()['guru'];
		$now = date_now();
		//dd(date_now());
		try {
			$password = password_hash('123456', PASSWORD_DEFAULT);
			$this->db->query("insert into t_guru (nip_guru,nama_guru,alamat,no_hp,email,akses,id_mapel) values ('$nip','$nama','$alamat','$no_hp','$email','1','$mapel')");
			$this->db->query("insert into t_user (username,nama,password,role,created_at) values ('$nip','$nama','$password','$role','$now')");
			//dd(123);
			$data = 1;
		} catch (\Throwable $th) {
			$data = $th;
		}
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($data);
	}

	public function update_guru() {
		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$email = $this->input->post('email');
		$mapel = $this->input->post('mapel');

		
		$q = "UPDATE t_guru SET nama_guru='$nama',alamat='$alamat',no_hp='$no_hp',email='$email',id_mapel='$mapel' WHERE nip_guru='$nip'";
		/*var_dump($q);
		//var_dump($nip);
		exit();*/
		$ret = $this->db->query($q);
		
		if ($ret == 1) {
			$data ="Sukses";
		} else {
			$data = "Gagal";
		}
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function delete_guru() {
		$nip = $this->input->post('id');
		$q = "delete from t_guru WHERE id_guru=$nip";
		/*var_dump($q);
		//var_dump($nip);
		exit();*/
		$ret = $this->db->query($q);
		
		if ($ret == 1) {
			$data ="Sukses";
		} else {
			$data = "Gagal";
		}
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function list_guru(){
		$result = $this->db->query("select * from t_guru")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function guru_by_nip(){
		$nip = $this->input->post('nip');
		$result = $this->db->query("select * from t_guru where nip_guru = ".$nip."")->row();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}


}
