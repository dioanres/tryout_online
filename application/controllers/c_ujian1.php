<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_ujian extends CI_Controller {

	
	public function index()
	{
		$this->load->view('v_ujian');
	}
	public function hasil_ujian()
	{
		$this->load->view('v_hasil_ujian');
	}
	public function proses_ujian()
	{
		$this->load->view('v_proses_ujian');
	}

	public function tampil_soal_ujian()
	{
		$this->load->view('v_soal_ujian');
	}

	public function list_ujian_siswa(){
		$id=$this->session->userdata('akses_login')->nama_admin;
		$result = $this->db->query("select a.id_pelajaran,a.nama_pelajaran,case when b.status='0' then 'Belum Ujian' when b.status = '1' then 'Sudah Ujian' else 'Belum Ujian' end status from t_mapel a left join t_hasil_ujian b on a.id_pelajaran = b.id_mapel and b.id_siswa='$id'")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
    }
}
