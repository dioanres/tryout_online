<?php 

session_start();
require __DIR__.'/../../vendor/autoload.php';
class C_login extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		//$this->load->model('models');
		$this->load->library('session');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function login()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|callback_username_check');
		$this->form_validation->set_rules('password', 'Password');
		
		if($this->form_validation->run() == FALSE)
			{
				$this->load->view('header');
				$this->load->view('login');
				$this->load->view('footer');
			}
		else
		if($this->session->userdata('akses'))
		{
			//exit();
			redirect(site_url('c_home'), 'refresh');
		}
		else
		{
			redirect(site_url('c_home'), 'refresh');
		}
	}

	public function username_check($user)
	{
		$pass = $this->input->post('password');
		if($user == '' && $pass == '')
		{
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Username dan Password Harus Diisi</div>');
			return FALSE;
		}
		elseif($user == '' || $user == null)
		{
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Username harus diisi</div>');
			return FALSE;
		}
		elseif ($pass == '' || $pass == null) {
			$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Password harus diisi</div>');
			return FALSE;
		}
		else
		{	
			
			$data_user=$this->db->query("SELECT * FROM t_user a 
											LEFT JOIN t_guru b on a.username = b.nip_guru
											WHERE a.username = '$user'")
											->row();
			//dd($data_user);
			if(empty($data_user)){
				$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Username atau Password Salah</div>');
				return FALSE;		   
				 
			} else {
				
				$verif_pass = password_verify($pass,$data_user->password);
				if ($verif_pass) {
					$this->session->set_userdata('akses_login', $data_user);
					return TRUE;
				} else {
					$this->form_validation->set_message('username_check', '<div class="alert alert-danger text-center">Password yang anda masukkan salah</div>');
					return FALSE;
				}
			}
			
		}
	}

	public function update_password()
	{
		$new_password = password_hash($this->input->post('new_password'), PASSWORD_DEFAULT);
		
		$id_user = $this->session->userdata('akses_login')->id;
		$q="update t_user SET password = '$new_password' WHERE id='$id_user'";
		$ret = $this->db->query($q);
		/*var_dump($q);
		exit();*/
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
		
	}
	public function logout() {
		$this->session->unset_userdata('akses');
		$this->session->sess_destroy();
		redirect(site_url('C_login'), 'refresh');
	}

	public function akses() {
		$data = $this->session->userdata('akses_login');
		
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($data));
	}
}
