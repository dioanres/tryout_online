<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';

class C_soal extends CI_Controller {

	
	public function index()
	{
		$this->load->view('v_soal');
	}

	public function tambah_soal($id_ujian = '')
	{
		$a['nama']=$this->session->userdata('akses_login');
		$a['ujian'] = $this->db->query("SELECT * FROM t_ujian u LEFT JOIN t_mapel m on u.id_pelajaran = m.id_pelajaran WHERE u.id_ujian = '$id_ujian'")->result();
		
		$this->load->view('v_tambah_soal',$a);
	}

	public function update_soal($id_ujian = '')
	{
		
		$a['nama']=$this->session->userdata('akses_login');
		$a['ujian'] = $this->db->query("SELECT * FROM t_ujian u LEFT JOIN t_mapel m on u.id_pelajaran = m.id_pelajaran WHERE u.id_ujian = '$id_ujian'")->result();
		/*var_dump($a['nama']);
		exit();*/
		$this->load->view('v_update_soal',$a);
	}

	public function tambah_text_soal() {
		$mapel = $this->input->post('mapel');
		$guru = $this->input->post('guru');
		$soal = substr($this->input->post('soal'), 3, strlen($this->input->post('soal')) - 8);
		$pil_a = substr($this->input->post('pil_a'), 3, strlen($this->input->post('pil_a')) - 8);
		$pil_b = substr($this->input->post('pil_b'), 3, strlen($this->input->post('pil_b')) - 8);
		$pil_c = substr($this->input->post('pil_c'), 3, strlen($this->input->post('pil_c')) - 8);
		$pil_d = substr($this->input->post('pil_d'), 3, strlen($this->input->post('pil_d')) - 8);

		$jawaban = $this->input->post('jawaban');
		$id_ujian = $this->input->post('id_ujian');

		$ret = $this->db->query("insert into t_soal (soal, pil_a, pil_b, pil_c, pil_d, jawaban, id_ujian) values ('$soal', '$pil_a', '$pil_b', '$pil_c', '$pil_d', '$jawaban','$id_ujian')");

		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function edit_text_soal() {
		$id_soal = $this->input->post('id_soal');
		$soal = substr($this->input->post('soal'), 3, strlen($this->input->post('soal')) - 8);
		$pil_a = substr($this->input->post('pil_a'), 3, strlen($this->input->post('pil_a')) - 8);
		$pil_b = substr($this->input->post('pil_b'), 3, strlen($this->input->post('pil_b')) - 8);
		$pil_c = substr($this->input->post('pil_c'), 3, strlen($this->input->post('pil_c')) - 8);
		$pil_d = substr($this->input->post('pil_d'), 3, strlen($this->input->post('pil_d')) - 8);

		$jawaban = $this->input->post('jawaban');

		$ret = $this->db->query(" UPDATE t_soal SET soal = '$soal', pil_a = '$pil_a',pil_b = '$pil_b', pil_c = '$pil_c', pil_d ='$pil_d', jawaban = '$jawaban' WHERE id_soal = '$id_soal'");
		
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function hapus_soal() {
		$id_soal = $this->input->post('id');
		
		$ret = $this->db->query("DELETE FROM t_soal WHERE id_soal = '$id_soal'");
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function list_soal(){
		$result = $this->db->query("select a.*,b.nama_pelajaran,c.nama_guru from t_soal a,t_mapel b,t_guru c where a.id_pelajaran=b.id_pelajaran and a.id_guru=c.nip_guru")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function list_soal_ujian(){
		$id_ujian = $this->input->post('id_ujian');
		$result = $this->db->query("select * from t_ujian u LEFT JOIN t_soal s on u.id_ujian = s.id_ujian LEFT JOIN t_mapel m on m.id_pelajaran = u.id_pelajaran where u.id_ujian = '$id_ujian' ORDER BY rand()")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function simpan_jawaban(){
		$jawaban = $this->input->post('jawaban');
		$id_ujian = $this->input->post('id_ujian');
		$id_siswa = $this->session->userdata('akses_login')->username;

		$sql = '';
		$benar = 0;

		foreach( $jawaban as $row ) {
		    if($row['jawaban'] == $row['kunci']){
		    	$benar++;
		    }
		}

		$score = ($benar / sizeof($jawaban)) * 100;

		$this->db->query("INSERT INTO t_hasil_ujian_head (id_ujian, id_siswa, score, status) VALUES ('$id_ujian', '$id_siswa', '$score', 1)");

		$hasil_id = (Int)$this->db->query("SELECT a.hasil_id FROM t_hasil_ujian_head a WHERE a.id_ujian = '$id_ujian' and a.id_siswa = '$id_siswa'")->result()[0]->hasil_id;

		foreach( $jawaban as $row ) {
		    $sql = $sql."(".$hasil_id.", '".$row['jawaban']."',".$row['soal']."),";
		}

		$sql = substr($sql, 0, strlen($sql)-1);

		$hasil = $this->db->query("INSERT INTO t_hasil_ujian (hasil_id, jawaban, id_soal) VALUES ".$sql);

        $this->output->set_output($hasil);
	}
}
