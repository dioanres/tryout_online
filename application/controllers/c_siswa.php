<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require __DIR__.'/../../vendor/autoload.php';
//include_once (dirname(__FILE__) . "/c_mapel.php");

class C_siswa extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		
		$this->load->model('model_mapel');
		

	}
	public function index()
	{
		$this->load->view('v_siswa');
		$this->load->view('modal_siswa');
	}

	public function tambah_siswa() {
		$kode = $this->input->post('kode');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$jurusan = $this->input->post('jurusan');
		$jk = $this->input->post('jk');
		$mapel = $this->input->post('mapel');
		$newDate = date("Y-m-d", strtotime($tgl_lahir));
		$role = role()['siswa'];
		$now = date_now();

		$data = array(
			"kd_siswa" => $kode,
			"nama_siswa" => $nama,
			"tgl_lahir_siswa" => $newDate,
			"alamat_siswa" => $alamat,
			"no_hp" => $no_hp,
			"jenis_kelamin" => $jk,
			"akses" => 2,
			"mapel" => json_encode($this->input->post('mapel'))
		);

		try {
			$password = password_hash('123456', PASSWORD_DEFAULT);
			
			$this->db->insert('t_siswa',$data);

			foreach ($mapel as $key => $value) {
				$q3 = "insert into siswa_has_mapel (id_siswa,id_mapel) values ('$kode','$value')";
				$ret3 = $this->db->query($q3);
			}
			

			$q2 = "insert into t_user (username,nama,password,role,created_at) values ('$kode','$nama','$password','$role','$now')";
			$ret2 = $this->db->query($q2);

			$data = 1;
		} catch (\Throwable $th) {
			$data = $th;
		}
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($data);
	}

	public function update_siswa() {
		$kode = $this->input->post('kode');
		$nama = $this->input->post('nama');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$jurusan = $this->input->post('jurusan');
		$jk = $this->input->post('jk');
		$newDate = date("Y-m-d", strtotime($tgl_lahir));
		$mapel = $this->input->post('mapel');

		$this->db->delete('siswa_has_mapel', array('id_siswa' => $kode));
		foreach ($mapel as $key => $value) {
			$q3 = "insert into siswa_has_mapel (id_siswa,id_mapel) values ('$kode','$value')";
			$ret3 = $this->db->query($q3);
		}

		$mapel = json_encode($mapel);

		$q="update t_siswa SET tgl_lahir_siswa = '$newDate',alamat_siswa='$alamat',no_hp='$no_hp',jenis_kelamin='$jk', mapel='$mapel' WHERE kd_siswa=$kode";
		/*var_dump($q);
		exit();*/
		$ret = $this->db->query($q);
		
		if ($ret == 1) {
			$data ="Sukses";
		} else {
			$data = "Gagal";
		}
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function delete_siswa() {
		$kd_siswa = $this->input->post('id');
		$q = "delete from t_siswa WHERE kd_siswa=$kd_siswa";
		
		$ret = $this->db->query($q);
		
		if ($ret == 1) {
			$data ="Sukses";
		} else {
			$data = "Gagal";
		}
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function list_siswa(){

		$result = $this->db->query("select * from t_siswa")->result();
		
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function list_mapel_by_id() {
		$id = json_decode($this->input->post('id'));
		//$id = ['7','8'];
		$res = $this->model_mapel->list_mapel_by_id($id);

		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($res));
		//dd($res);
	}
	public function siswa_by_id(){
		$kd_siswa = $this->input->post('kd_siswa');
		$result = $this->db->query("select * from t_siswa where kd_siswa = ".$kd_siswa."")->row();
		$result->mapel = json_decode($result->mapel);
		
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

}
