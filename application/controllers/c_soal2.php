<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_soal extends CI_Controller {

	
	public function index()
	{
		$this->load->view('v_soal');
	}

	public function tambah_soal()
	{
		$a['nama']=$this->session->userdata('akses_login');
		/*var_dump($a['nama']);
		exit();*/
		$this->load->view('v_tambah_soal',$a);
	}

	public function tambah_text_soal() {
		$mapel = $this->input->post('mapel');
		$guru = $this->input->post('guru');
		$soal = $this->input->post('soal');
		$pil_a = $this->input->post('pil_a');
		$pil_b = $this->input->post('pil_b');
		$pil_c = $this->input->post('pil_c');
		$pil_d = $this->input->post('pil_d');
		$jawaban = $this->input->post('jawaban');

		$ret = $this->db->query("insert into t_soal (id_guru, soal, pil_a, pil_b, pil_c, pil_d, jawaban, id_ujian, waktu) values ('$guru', '$soal', '$pil_a', '$pil_b', '$pil_c', '$pil_d', '$jawaban','','')");
		//$ret2 = $this->db->query("insert into t_admin (kd_admin,nama_admin,akses) values ('$nip','$nama','1')");
		/*if ($ret == 1 &&) {
			$data ="Sukses";
		} else {
			$data = "Gagal";
		}*/
		
		$this->output->set_content_type('application/text');
        $this->output->set_output($ret);
	}

	public function list_soal(){
		$result = $this->db->query("select a.*,b.nama_pelajaran,c.nama_guru from t_soal a,t_mapel b,t_guru c where a.id_pelajaran=b.id_pelajaran and a.id_guru=c.nip_guru")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}

	public function list_soal_ujian(){
		$id_pelajaran=$this->input->post('id_pelajaran');
		$result = $this->db->query("select * from t_soal")->result();
		//$data = array($result);
		/*print_r($data);
		exit();*/
		$this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));
	}
}
