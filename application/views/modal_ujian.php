<!-- Modal -->
<div id="mod-ujian" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header mdl-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title white">Tambah List Ujian</h4>
            </div>
            <div class="modal-body mdl-tambah-ujian">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">Nama Ujian</label>
                        <div class="col-sm-9">
                            <input type="text" id="nama-ujian" placeholder="Nama Ujian" class="col-xs-10 col-sm-10" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">Mata Pelajaran</label>
                        <div class="col-sm-9">
                            <select class="chosen-select col-xs-10 col-sm-10" id="mapel-ujian">

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="form-field-1">Waktu</label>
                        <div class="col-sm-9">
                            <input type="text" maxlength="3" id="waktu" placeholder="Waktu/Menit"
                                class="col-xs-10 col-sm-10 inp-number" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <button type="button" id="simpan-mapel-ujian" class="btn btn-primary"
                                data-dismiss="modal">Simpan</button>
                        </div>

                        <!-- <label class="col-sm-3 control-label" for="form-field-1">Nama</label>
            <div class="col-sm-9">
              <input type="text" id="form-field-1" placeholder="Nama" class="col-xs-10 col-sm-5" />
            </div> -->
                    </div>
                </form>
            </div>
            <!-- <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
        </div>

    </div>
</div>
