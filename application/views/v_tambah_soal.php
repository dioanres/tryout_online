<!DOCTYPE html>
<html lang="en">
	<head>
	<?php $this->load->View('header');?>

	</head>

	<body class="no-skin">
		<?php $this->load->View('nav_bar');?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>


			<?php $this->load->View('side_bar');?>
			<div class="main-content">
				<div class="main-content-inner">

					<div class="page-content">

						<div class="page-header">
							<h1 id = "judul-tambah-soal">
								Tambah Soal <?php echo $ujian[0]->nama_ujian . ' ' . $ujian[0]->nama_pelajaran; ?>
								<!-- <small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small> -->
							</h1>
						</div>
						<div class="row">
							<form class="form-horizontal soal-form">
								 <div class="form-group">
								<!--<div class="col-sm-2">
								<label>Mata Pelajaran</label>
								</div>
								<div class="col-sm-8">
									<select class="chosen-select form-control" id="slc-pelajaran-soal" data-placeholder="Choose a State...">

									</select>
								</div> -->
							</div>
							<div class="form-group">
								<div class="col-sm-2">
									<label>Guru</label>
								</div>
								<div class="col-sm-8">
									<input type="text" id="txt-guru-soal" class="col-xs-10 col-sm-10" value="<?php echo $nama->username . " - " . $nama->nama; ?>" disabled />
									<!-- <select class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose a State...">
										<option value="">  </option>
										<option value="AL">AGUS</option>
										<option value="AK">BUDI</option>
										<option value="AZ">AHMAD</option>
										<option value="AR">ERI</option>
									</select> -->
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-2">
									<label>Text Soal</label>
								</div>
								<div class="col-sm-8">
									<textarea class="ckeditor" id="txt-soal" name="txt-soal"></textarea>
									<!-- <div class="wysiwyg-editor" contenteditable="true"></div> -->
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-2">
									<label>Pilihan A</label>
								</div>
								<div class="col-sm-8">
									<textarea class="ckeditor" id="pilihan-a" name="pilihan-a"></textarea>
									<!-- <input class = "input-jawaban" type="text" id="pilihan-a"> -->
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-2">
									<label>Pilihan B</label>
								</div>
								<div class="col-sm-8">
									<textarea class="ckeditor" id="pilihan-b" name="pilihan-b"></textarea>
									<!-- <input class = "input-jawaban" type="text" id="pilihan-b"> -->
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-2">
									<label>Pilihan C</label>
								</div>
								<div class="col-sm-8">
									<textarea class="ckeditor" id="pilihan-c" name="pilihan-c"></textarea>
									<!-- <input class = "input-jawaban" type="text" id="pilihan-c"> -->
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-2">
									<label>Pilihan D</label>
								</div>
								<div class="col-sm-8">
									<textarea class="ckeditor" id="pilihan-d" name="pilihan-d"></textarea>
									<!-- <input class = "input-jawaban" type="text" id="pilihan-d"> -->
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-2">
									<label>Kunci Jawaban</label>
								</div>
								<div class="col-sm-2">
									<select class="chosen-select form-control" id="jawaban" data-placeholder="Choose a State...">
										<option value="">  </option>
										<option value="A">A</option>
										<option value="B">B</option>
										<option value="C">C</option>
										<option value="D">D</option>
									</select>
								</div>
							</div>

							<div class="form-group">
					            <div class="col-sm-2"></div>
					            <div class="col-sm-9">
					              <button type="button" class="btn btn-primary" id="btn-simpan-soal">Simpan</button>
					            </div>
					        </div>
							</form>

						</div>
						<!-- /.page-content -->
					</div>
				</div><!-- /.main-content -->

			<?php $this->load->View('footer_content');?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<?php $this->load->View('footer');?>
	</body>
</html>
