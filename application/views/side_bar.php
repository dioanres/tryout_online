<div class="sidebar-shortcuts" id="sidebar-shortcuts">

				<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li class="active" id="menu-dashboard">
						<a href="<?php echo base_url();?>index.php/c_home">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>
						<b class="arrow"></b>
					</li>

					

					<li class="" id="menu-guru" style="display:none">
						<a href="<?php echo base_url(); ?>index.php/c_guru">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Data Guru </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="" id="menu-siswa" style="display:none">
						<a href="<?php echo base_url(); ?>index.php/c_siswa">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Data Siswa </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="" id="menu-mapel" style="display:none">
						<a href="<?php echo base_url(); ?>index.php/c_mapel">
							<i class="menu-icon fa fa-book"></i>
							<span class="menu-text"> Data Mapel </span>
						</a>

						<b class="arrow"></b>
					</li>
					<!-- <li class="" id="menu-soal" style="display:none">
						<a href="<?php //echo base_url(); ?>index.php/c_soal">
							<i class="menu-icon fa fa-book"></i>
							<span class="menu-text"> Soal </span>
						</a>

						<b class="arrow"></b>
					</li> -->
					<li class="" id="menu-ujian" style="display:none">
						<a href="<?php echo base_url(); ?>index.php/c_ujian">
							<i class="menu-icon fa fa-file"></i>
							<span class="menu-text"> Ujian </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="" id="menu-hasil-ujian" style="display:none">
						<a href="<?php echo base_url(); ?>index.php/c_ujian/hasil_ujian">
							<i class="menu-icon fa fa-file"></i>
							<span class="menu-text"> Hasil Ujian </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="" id="menu-proses-ujian" style="display:none">
						<a href="<?php echo base_url(); ?>index.php/c_ujian/proses_ujian">
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text"> Proses Ujian </span>
						</a>

						<b class="arrow"></b>
					</li>
					<li class="" id="report-ujian" style="display:none">
						<a href="<?php echo base_url(); ?>index.php/c_ujian/proses_ujian">
							<i class="menu-icon fa fa-file"></i>
							<span class="menu-text"> Report Ujian </span>
						</a>

						<b class="arrow"></b>
					</li>

					

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>