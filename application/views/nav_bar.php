<div id="navbar" class="navbar navbar-default          ace-save-state">
	<div class="navbar-container ace-save-state" id="navbar-container">
		<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
			<span class="sr-only">Toggle sidebar</span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>
		</button>

		<div class="navbar-header pull-left">
			<a href="index.html" class="navbar-brand">
				<small>
					<i class="fa fa-leaf"></i>
					Ujian Try Out Online
				</small>
			</a>
		</div>

		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">
				<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<!-- <img class="nav-user-photo" src="<?php echo base_url(); ?>assets/images/avatars/user.jpg" alt="Jason's Photo" /> -->
						<span class="user-info">
							<small>Welcome,</small>
							<label id="nm_login"></label>
						</span>

						<i class="ace-icon fa fa-caret-down"></i>
					</a>

					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<!-- <li>
							<a href="#">
								<i class="ace-icon fa fa-cog"></i>
								Settings
							</a>
						</li>

						 -->

						<!-- <li class="divider"></li> -->
						<li>
							<a href="#" data-toggle="modal" data-target="#mod-password">
								<i class="ace-icon fa fa-cog"></i>
								Ganti Password
							</a>
						</li>
						<li id="log-out">
							<a href="<?php echo base_url();?>index.php/c_login/logout">
								<i class="ace-icon fa fa-power-off"></i>
								Logout
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div><!-- /.navbar-container -->
</div>

<!-- Modal -->
<div id="mod-password" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header mdl-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title white">Update Password</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">New Password</label>
            <div class="col-sm-9">
              <input type="password" id="new-password" placeholder="New Password" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Confirm Password</label>
            <div class="col-sm-9">
              <input type="password" id="confirm-new-password" placeholder="Confirm Password" class="col-xs-10 col-sm-10" />
            </div>
          </div>
		  <div class="col-sm-3"></div>
		  <div class ='col-sm-9 errors'>
		  	
		  </div>
			<br>
			<br>
          
        </form>
      </div>
	  <div class="modal-footer">
	  	<div class="form-group">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
              <button type="button" id="update-password" class="btn btn-primary">Simpan</button>
            </div>
        </div>
	  </div>
    </div>

  </div>
</div>