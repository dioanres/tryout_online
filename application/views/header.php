<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /> -->
<!-- <meta charset="utf-8" /> -->
<title>Dashboard - Ace Admin</title>

<meta name="description" content="overview &amp; stats" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

<!-- bootstrap & fontawesome -->

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-timepicker.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.min.css" />

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/4.5.0/css/font-awesome.min.css" />

<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-confirm/dist/jquery-confirm.min.css">

<!-- text fonts -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fonts.googleapis.com.css" />

<!-- ace styles -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

<!--[if lte IE 9]>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-rtl.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/new.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
<!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/select2.min.css" /> -->
<link rel="stylesheet" href="<?= base_url(); ?>assets/select2-4.0.10/dist/css/select2.min.css" />
<!--[if lte IE 9]>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-ie.min.css" />
<![endif]-->

<!-- inline styles related to this page -->

<!-- ace settings handler -->
<script src="<?php echo base_url(); ?>assets/js/ace-extra.min.js"></script>