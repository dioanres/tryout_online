<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->View('header'); ?>

</head>
<style>
.infobox {
    height: 90px
}
</style>

<body class="no-skin">
    <?php $this->load->View('nav_bar'); ?>

    <div class="main-container ace-save-state" id="main-container">
        <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {}
        </script>

        <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
            <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {}
            </script>


            <?php $this->load->View('side_bar'); ?>
            <div class="main-content">
                <div class="main-content-inner">
                    <!--  -->
                    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                        <ul class="breadcrumb">
                            <li>
                                <i class="ace-icon fa fa-home home-icon"></i>
                                <a href="#">Home</a>
                            </li>
                            <li class="active">Dashboard</li>
                        </ul><!-- /.breadcrumb -->

                        <div class="nav-search" id="nav-search">
                            <form class="form-search">
                                <span class="input-icon">
                                    <input type="text" placeholder="Search ..." class="nav-search-input"
                                        id="nav-search-input" autocomplete="off" />
                                    <i class="ace-icon fa fa-search nav-search-icon"></i>
                                </span>
                            </form>
                        </div><!-- /.nav-search -->
                    </div>

                    <h4>Selamat Datang <b><?php echo $akses_login->nama; ?></b> Di Aplikasi Ujian Try Out Online</h4>

                    <hr />
                    <!-- /.page-content -->
                    <div class="row">
                        <div class="space-12"></div>

                        <div class="col-sm-12 infobox-container">
                            <div class="infobox infobox-green">
                                <div class="infobox-icon">
                                    <i class="ace-icon fa fa-user"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $guru ?></span>
                                    <div class="infobox-content">Total Guru</div>
                                </div>
                            </div>

                            <div class="infobox infobox-blue">
                                <div class="infobox-icon">
                                    <i class="ace-icon fa fa-user"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $siswa ?></span>
                                    <div class="infobox-content">Total Siswa</div>
                                </div>
                            </div>

                            <div class="infobox infobox-pink">
                                <div class="infobox-icon">
                                    <i class="ace-icon fa fa-copy"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number"><?= $ujian ?></span>
                                    <div class="infobox-content">Total Ujian</div>
                                </div>
                            </div>

                            <div class="infobox infobox-red">
                                <div class="infobox-icon">
                                    <i class="ace-icon fa fa-copy"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">7</span>
                                    <div class="infobox-content">Mata Pelajaran</div>
                                </div>
                            </div>

                            <div class="infobox infobox-red">
                                <div class="infobox-icon">
                                    <i class="ace-icon fa fa-copy"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">7</span>
                                    <div class="infobox-content">Siswa Lulus</div>
                                </div>
                            </div>

                            <div class="infobox infobox-red">
                                <div class="infobox-icon">
                                    <i class="ace-icon fa fa-copy"></i>
                                </div>

                                <div class="infobox-data">
                                    <span class="infobox-data-number">7</span>
                                    <div class="infobox-content">Siswa Tidak Lulus</div>
                                </div>
                            </div>
                            <div class="space-6"></div>
                        </div>

                        <div class="vspace-12-sm"></div>
                    </div><!-- /.row -->
                </div>
            </div><!-- /.main-content -->


            <?php $this->load->View('footer_content'); ?>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div><!-- /.main-container -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <?php $this->load->View('footer'); ?>
</body>

</html>