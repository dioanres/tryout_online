<!-- Modal -->
<div id="mod-mapel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header mdl-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title white">Tambah Mata Pelajaran</h4>
      </div>
      <div class="modal-body mdl-tambah-mapel">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Nama</label>
            <div class="col-sm-9">
              <input type="text" id="nama-mapel" placeholder="Nama Mapel" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
              <button type="button" id="simpan-mapel" class="btn btn-primary" data-dismiss="modal">Simpan</button>
            </div>
            
            <!-- <label class="col-sm-3 control-label" for="form-field-1">Nama</label>
            <div class="col-sm-9">
              <input type="text" id="form-field-1" placeholder="Nama" class="col-xs-10 col-sm-5" />
            </div> -->
          </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>