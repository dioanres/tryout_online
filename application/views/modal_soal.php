<!-- Modal -->
<div id="modal-soal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header mdl-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title white">Tambah Mata Pelajaran</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <div class="col-sm-2">
              <label>Text Soal</label>
            </div>
            <div class="col-sm-8">
              <textarea class="ckeditor" id="txt-soal" name="txt-soal"></textarea>
              <!-- <div class="wysiwyg-editor" contenteditable="true"></div> -->
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-2">
              <label>Pilihan A</label>
            </div>
            <div class="col-sm-8">
              <textarea class="ckeditor" id="pilihan-a" name="pilihan-a"></textarea>
              <!-- <input class = "input-jawaban" type="text" id="pilihan-a"> -->
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label>Pilihan B</label>
            </div>
            <div class="col-sm-8">
              <textarea class="ckeditor" id="pilihan-b" name="pilihan-b"></textarea>
              <!-- <input class = "input-jawaban" type="text" id="pilihan-b"> -->
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label>Pilihan C</label>
            </div>
            <div class="col-sm-8">
              <textarea class="ckeditor" id="pilihan-c" name="pilihan-c"></textarea>
              <!-- <input class = "input-jawaban" type="text" id="pilihan-c"> -->
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label>Pilihan D</label>
            </div>
            <div class="col-sm-8">
              <textarea class="ckeditor" id="pilihan-d" name="pilihan-d"></textarea>
              <input class = "" type="text" id="id-soal-hidden">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <label>Kunci Jawaban</label>
            </div>
            <div class="col-sm-2">
              <select class="chosen-select form-control" id="jawaban" data-placeholder="Choose a State...">
                <option value="">  </option>
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-10"></div>
            <div class="col-sm-2">
              <button type="button" id="btn-edit-soal" class="btn btn-primary" data-dismiss="modal">Simpan</button>
            </div>
            
            <!-- <label class="col-sm-3 control-label" for="form-field-1">Nama</label>
            <div class="col-sm-9">
              <input type="text" id="form-field-1" placeholder="Nama" class="col-xs-10 col-sm-5" />
            </div> -->
          </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>