<!DOCTYPE html>
<html lang="en">
	<head>
	<?php $this->load->View('header');?>

	</head>

	<body class="no-skin">
		<?php $this->load->View('nav_bar');?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar responsive ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>


			<?php $this->load->View('side_bar');?>
			<div class="main-content">
				<div class="main-content-inner">

					<div class="page-content">

						<div class="page-header">
							<h1 id = "judul-tambah-soal">
								List Soal <?php echo $ujian[0]->nama_ujian . ' ' . $ujian[0]->nama_pelajaran; ?>
								<!-- <small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small> -->
							</h1>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div>
								<table id="tbl-soal" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Soal</th>
											<!-- <th>Pilihan A</th>
											<th>Pilihan B</th>
											<th>Pilihan C</th>
											<th>Pilihan D</th> -->
											<th>Aksi</th>
										</tr>
									</thead>

									<tbody>

									</tbody>
								</table>
							</div>
							</div>
						</div>

						</div>
						<!-- /.page-content -->
					</div>
				</div><!-- /.main-content -->

			<?php $this->load->view('modal_soal');
$this->load->View('footer_content');?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<?php $this->load->View('footer');?>
	</body>
</html>
