<?php $this->load->library('session'); ?>
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.min.js"></script>


<!-- <script src="<?php //echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script> -->

<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>


<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
  <script src="<?php echo base_url(); ?>assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>assets/jquery-confirm/dist/jquery-confirm.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.index.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.flot.resize.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/bootstrap-timepicker.min.js"></script>



<!-- data tables -->
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/dataTables.select.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.min.js"></script>
<!-- ace scripts -->
<script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>
<!-- <script src="<?= base_url(); ?>assets/js/select2.min.js"></script> -->
<script src="<?= base_url(); ?>assets/select2-4.0.10/dist/js/select2.min.js"></script>
<script src="<?= base_url(); ?>assets/js/jquery-typeahead.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/function.js"></script>



<!-- inline scripts related to this page -->
<script type="text/javascript">

	var base_url = "<?= base_url() ?>";
	var session_mapel_guru = "<?= $this->session->userdata('akses_login')->id_mapel ?>";
		
		//show the dropdowns on top or bottom depending on window height and menu position
	$('#task-tab .dropdown-hover').on('mouseenter', function(e) {
		var offset = $(this).offset();

		var $w = $(window)
		if (offset.top > $w.scrollTop() + $w.innerHeight() - 100) 
			$(this).addClass('dropup');
		else $(this).removeClass('dropup');
	});
	//CKEDITOR.replace('#txt-soal');

	//inisiasi data table
	$(document).ready(function(){

		$('#warna').select2({
			allowClear:true,
			tags:true
		});

		$('.select2').css('width','350px').select2({allowClear:true})
		$('#select2-multiple-style .btn').on('click', function(e){
		var target = $(this).find('input[type=radio]');
		var which = parseInt(target.val());
		if(which == 2) $('.select2').addClass('tag-input-style');
			else $('.select2').removeClass('tag-input-style');
	});

	var substringMatcher = function(strs) {
		return function findMatches(q, cb) {
			var matches, substringRegex;
			
			// an array that will be populated with substring matches
			matches = [];
			
			// regex used to determine if a string contains the substring `q`
			substrRegex = new RegExp(q, 'i');
			
			// iterate through the pool of strings and for any string that
			// contains the substring `q`, add it to the `matches` array
			$.each(strs, function(i, str) {
				if (substrRegex.test(str)) {
					// the typeahead jQuery plugin expects suggestions to a
					// JavaScript object, refer to typeahead docs for more info
					matches.push({ value: str });
				}
			});

			cb(matches);
		}
	}

	$('input.typeahead').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	}, {
		name: 'states',
		displayKey: 'value',
		source: substringMatcher(ace.vars['US_STATES']),
		limit: 10
	});	
				
		
		$('.date-picker').datepicker({
		//console.log('date picker');
		//format: 'YYYY-mm-dd',
		autoclose: true,
		todayHighlight: true
		})
		//show datepicker when clicking on the icon
		.next().on(ace.click_event, function(){
			console.log(this);
			$(this).prev().focus();
		});

		$(".inp-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	});

	$('.soal-form .wysiwyg-editor').ace_wysiwyg({
		toolbar:
		[
			'bold',
			'italic',
			'strikethrough',
			'underline',
			null,
			'justifyleft',
			'justifycenter',
			'justifyright',
			null,
			'createLink',
			'unlink',
			null,
			'undo',
			'redo'
		]
	}).prev().addClass('wysiwyg-style1');

	
	
				
	//in ajax mode, remove remaining elements before leaving page
	$(document).one('ajaxloadstart.page', function(e) {
		$('[class*=select2]').remove();
		$('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
		$('.rating').raty('destroy');
		$('.multiselect').multiselect('destroy');
	});

	$('#update-password').click(function(){
		var new_password = $('#new-password').val();
		var confirm_password = $('#confirm-new-password').val();

		if (new_password != confirm_password) {
			$('.errors').html('<span class="text-danger"> Password dan Konfirmasi Password tidak sesuai </span>');
		} else {
			$.ajax({
				url: base_url + 'index.php/c_login/update_password',
				type: 'post',
				dataType: 'json',
				async: false,
				data: {
					new_password: new_password,
					confirm_password: confirm_password
				},
				cache: false,
				success: function(response, status, error, pesan) {
					console.log(response);
					if(response == 1) {
						$('#mod-password').modal('hide');
						alert_info('Info', 'Password berhasil diganti');
					}
				},
				error: function(response) {
					console.log(response['responseText']);

					alert_info('Error', response['responseText']);

				}
			});
		}
		
	});
</script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/main.js"></script>