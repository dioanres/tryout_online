<!DOCTYPE html>
<html lang="en">
	<head>
	<?php $this->load->View('header'); ?>	

	</head>

	<body class="no-skin">
		<?php $this->load->View('nav_bar'); ?>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				
			<?php $this->load->View('side_bar'); ?>
			<div class="main-content">
				<div class="main-content-inner">
					

					<div class="page-content">
						<div class="row">
	<div class="col-xs-12">
		<h3 class="header smaller lighter blue">Data Mata Pelajaran</h3>

		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>
		<button class="btn btn-success" id="btn-tambah-mapel" type="button" data-toggle="modal" data-target="#mod-mapel">
			<i class="ace-icon fa fa-plus bigger-110"></i>
			Tambah
		</button>
		<div class="clearfix">
			<div class="pull-right tableTools-container"></div>
		</div>
		<div class="table-header">
			List Data Mata Pelajaran
		</div>

		<!-- div.table-responsive -->

		<!-- div.dataTables_borderWrap -->
		<div>
			<table id="tbl-mapel" class="table table-striped table-bordered table-hover">
				<thead>
					<tr>
						<th>No</th>
						<th>Id Mapel</th>
						<th>Nama</th>
						<th>Aksi</th>
					</tr>
				</thead>

				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>

						<!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<?php $this->load->View('footer_content'); ?>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<?php $this->load->View('footer'); ?>
	</body>
</html>
