<!-- Modal -->
<div id="mod-guru" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header mdl-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title white">Tambah Guru</h4>
      </div>
      <div class="modal-body mdl-tambah-guru">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">NIP</label>
            <div class="col-sm-9">
              <input type="text" id="nip-guru" placeholder="Username" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Nama</label>
            <div class="col-sm-9">
              <input type="text" id="nama" placeholder="Nama" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Alamat</label>
            <div class="col-sm-9">
              <input type="text" id="alamat" placeholder="Alamat" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">No HP</label>
            <div class="col-sm-9">
              <input type="text" id="no-hp" placeholder="No Hp" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Email</label>
            <div class="col-sm-9">
              <input type="text" id="email" placeholder="Email" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Mata Pelajaran</label>
            <div class="col-sm-9">
              <select class="chosen-select form-control" id="mapel" data-placeholder="Choose a State...">
                    <!-- <option value="">  </option>
                    <option value="AL">IPA</option>
                    <option value="AK">IPS</option>
                    <option value="AZ">AHMAD</option>
                    <option value="AR">ERI</option> -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
              <button type="button" id="btn-simpan-guru" class="btn btn-primary" data-dismiss="modal">Simpan</button>
            </div>
            
            <!-- <label class="col-sm-3 control-label" for="form-field-1">Nama</label>
            <div class="col-sm-9">
              <input type="text" id="form-field-1" placeholder="Nama" class="col-xs-10 col-sm-5" />
            </div> -->
          </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>