<!-- Modal -->
<div id="mod-siswa" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header mdl-header">
        <button type="button" class="close" data-dismiss="modal" id="btn-tambah-siswa">&times;</button>
        <h4 class="modal-title white">Tambah Siswa</h4>
      </div>
      <div class="modal-body mdl-tambah-siswa">
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Kode Siswa</label>
            <div class="col-sm-9">
              <input type="text" id="kode-siswa" placeholder="Kode Siswa" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Nama</label>
            <div class="col-sm-9">
              <input type="text" id="nama-siswa" placeholder="Username" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <!-- <div class="input-group">
            <input class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="dd-mm-yyyy" />
            <span class="input-group-addon">
              <i class="fa fa-calendar bigger-110"></i>
            </span>
          </div> -->
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Tanggal Lahir</label>
            <div class="col-sm-9">
              <input type="text" id="tgl-lahir" placeholder="Tanggal Lahir" class="col-xs-10 col-sm-10 date-picker" data-date-format="dd-mm-yyyy" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Alamat</label>
            <div class="col-sm-9">
              <textarea class="col-xs-10 col-sm-10" id="alamat-siswa" placeholder="Alamat"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">No Hp</label>
            <div class="col-sm-9">
              <input type="text" id="no-hp-siswa" placeholder="No HP" class="col-xs-10 col-sm-10" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="form-field-1">Jenis Kelamin</label>
            <div class="col-sm-9">
              <div class="radio">
                <label>
                  <input name="jk" type="radio" id="jk-l" class="ace" value="L" />
                  <span class="lbl"> Laki-laki</span>
                </label>
              </div>
              <div class="radio">
                <label>
                  <input name="jk" type="radio" id="jk-p" class="ace" value="P" />
                  <span class="lbl"> Perempuan</span>
                </label>
              </div>
              <!-- <input type="text" id="form-field-1" placeholder="Nim" class="col-xs-10 col-sm-10" /> -->
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-3 control-label">Mata Pelajaran</label>
            <div class="col-sm-9">
            <select multiple="multiple" id="mapel-siswa" name="mapel[]" class="select2 col-xs-10 col-sm-10" data-placeholder="Pilih Mapel...">
              <option value="">--Pilih--</option>
            </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-3"></div>
            <div class="col-sm-9">
              <button type="button" id="btn-simpan-siswa" class="btn btn-primary" data-dismiss="modal">Simpan</button>
            </div>
            
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>